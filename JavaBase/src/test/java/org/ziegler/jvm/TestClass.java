package org.ziegler.jvm;

import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class TestClass {
    private int m;
    public int inc() {
        return m + 1;
    }

    public static int scall() throws Exception {
        int x;
        try {
            x = 1;
            return x;
        } catch (Exception e) {
            x = 2;
            return x;
        } finally {
            x = 3;
        }
    }

    public <R extends Integer, T> R get(T t) {
        return (R)t;
    }

    public static class InnerTestClass {
        private final int n;

        public InnerTestClass(int n) {
            this.n = n;
        }
    }

    /**
     * // -parameters 编译参数
     * @param value
     * @param extra
     * @return
     */
    public int add(int value, final String extra) {
        return ++value;
    }

    void doSomething(){

    }

    void onlyMe(TestClass t) {
        synchronized (t) {
            doSomething();
        }
    }

}
