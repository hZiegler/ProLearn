package org.ziegler.jvm.cp8;

public class TestVar {


    /**
     * -verbose:gc
     */
    public static void main(String[] args) {
        byte[] placeholder = new byte[64 * 1024 * 1024];
        System.gc();
        System.out.println(placeholder);
    }

}
