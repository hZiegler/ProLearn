package org.ziegler.jvm;

/**
 * VM参数：-XX:+UseG1GC -Xms20M -Xmx20M -Xmn10M -Xlog:gc* -XX:SurvivorRatio=8
 * */
public class FirstTestGC {

    public static void main(String[] args) {
        byte[] a1, a2, a3, a4;
        a1 = new byte[2 * 1024 * 1024];
        a2 = new byte[2 * 1024 * 1024];
        a3 = new byte[2 * 1024 * 1024];
        a4 = new byte[4 * 1024 * 1024];

        final int i = a1[0] + a2[0] + a3[0];
        System.out.println(i);
    }
}
