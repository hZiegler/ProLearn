package org.ziegler.jvm;

/**
 * VM参数：-XX:+UseG1GC -Xms20M -Xmx20M -Xlog:gc*
 * */
public class GCAllocAndRelease {
    public static void main(String[] args) {
        byte[] a1, a2, a3, a4, a5, a6, a7;
        a1 = new byte[2 * 1024 * 1024];
        a2 = new byte[2 * 1024 * 1024];
        a3 = new byte[2 * 1024 * 1024];
        a1 = null;

        a4 = new byte[2 * 1024 * 1024];
        a5 = new byte[2 * 1024 * 1024];
        a6 = new byte[2 * 1024 * 1024];
        a4 = null;
        a5 = null;
        a6 = null;
        a7 = new byte[2 * 1024 * 1024];

    }
}
