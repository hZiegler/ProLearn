package org.ziegler.jvm.cp7;

public class SubClass extends SuperClass {

    public static final int NUM = 888;

    static {
        System.out.println("SubClass.static initializer");
    }

    public SubClass() {
        System.out.println("SubClass.SubClass");
    }
}
