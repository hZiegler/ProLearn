package org.ziegler.jvm.cp7;

public class TestInit {

    // -XX:+TraceClassLoading
    @org.junit.jupiter.api.Test
    void test1() {
        System.out.println(SubClass.value);
    }

    @org.junit.jupiter.api.Test
    void test2() {
        SuperClass[] sca = new SuperClass[10];
    }

    @org.junit.jupiter.api.Test
    void test3() {
        System.out.println(SubClass.NUM);
    }

    @org.junit.jupiter.api.Test
    void test4() {
        System.out.println(SubClass.N);
    }
}
