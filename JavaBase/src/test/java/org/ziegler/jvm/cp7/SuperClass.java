package org.ziegler.jvm.cp7;

public class SuperClass {
    static {
        System.out.println("SuperClass.static initializer");
    }

    public static int value = 123;

//    public static final int value = 123;

    public static final int N = 382;

    public SuperClass() {
        System.out.println("SuperClass.SuperClass");
    }
}
