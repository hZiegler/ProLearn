package org.ziegler.jvm.cp7;

public class ConstClass {
    static {
        System.out.println("ConstClass.static initializer");
    }

    public static final String HELLOWORLD = "hello world";
}
