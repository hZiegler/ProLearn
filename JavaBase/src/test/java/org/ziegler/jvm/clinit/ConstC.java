package org.ziegler.jvm.clinit;

public class ConstC {
    public static int N1 = 11;
//    public static int N1 = 11;
    public static final int N2 = 22;
    public static final int N3 = 33;
    public static final int MAX = EnumType.MaxEnumType + 1;

    static {
//        System.out.println("ConstC.static initializer");
    }
}
