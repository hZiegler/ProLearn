package org.ziegler.jvm.clinit;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Arrays;

public enum EnumType {
    AA(ConstC.N1),
    BB(ConstC.N2),
    CC(ConstC.N3),
    ;

    public static int MaxEnumType;
    static {
//        System.out.println("EnumType.static initializer");
        MaxEnumType = Arrays.stream(values()).mapToInt(EnumType::getIndex).max()
                .orElseThrow(ExceptionInInitializerError::new);
    }


    public final int index;
    EnumType(int index) {
        this.index = index;
//        System.out.println("index = " + index);
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("index", index)
                .toString();
    }
}
