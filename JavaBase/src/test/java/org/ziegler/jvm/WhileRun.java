package org.ziegler.jvm;

public class WhileRun {

    public static void main(String[] args) {
        int runCount = 0;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000);

                System.out.println("[" + (++runCount) + "]running...");
            } catch (InterruptedException e) {
                // 线程终止，跳出循环
                System.out.println("InterruptedException线程终止，跳出循环 = " + e.getMessage());
                e.printStackTrace();
                break;
            }
        }

        System.out.println("Program terminate.");
    }
}
