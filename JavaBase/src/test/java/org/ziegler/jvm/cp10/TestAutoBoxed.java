package org.ziegler.jvm.cp10;

public class TestAutoBoxed {

    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        Integer c = 3;
        Integer d = 3;
        Integer e = 321;
        Integer f = 321;
        int ee = 321;
        Long g = 3L;
        System.out.println(c == d);
        System.out.println(e == f);
        System.out.println(e == ee);
        System.out.println(f == ee);
        System.out.println(e.equals(f));
        System.out.println(c == (a + b));
        System.out.println(e.equals(a + b));
        System.out.println(g == (a + b));
        System.out.println(g.equals(a + b));

        if (false) {
            System.out.println("dd");
        }
    }
}
