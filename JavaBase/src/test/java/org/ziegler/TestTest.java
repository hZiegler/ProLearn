package org.ziegler;

import org.junit.jupiter.api.Test;
import org.ziegler.javabase.thread.DelayUtil;

class TestTest {


    @Test
    void name() {
        Thread thread = new Thread(() -> {
            System.out.println("TestTest.name");


            long i = 0;
            try {
                while (true) {
                    ++i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                System.out.println("TestTest.name 2" + i);
            }


        });

        thread.start();

        DelayUtil.second(1);
//        thread.interrupt();
        thread.stop();

        DelayUtil.second(1000);
    }

    @Test
    void testDivide() {
        int a = 25;
        int b = 8;
        final int i = a / b;
        System.out.println("i = " + i);
    }

    @Test
    void testDivide1() {
        int a = 24;
        int b = 8;
        final int i = a / b;
        System.out.println("i = " + i);
    }

    @Test
    void testDivide2() {
        int a = 23;
        int b = 8;
        final int i = a / b;
        System.out.println("i = " + i);
    }
}