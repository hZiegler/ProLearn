package org.ziegler.zunit;

public interface ZUnitHumanObj extends ZUnit {
    void test(HumanObject humanObj);
}
