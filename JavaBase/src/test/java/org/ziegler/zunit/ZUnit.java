package org.ziegler.zunit;

/**
 * 测试单元
 * @author Ziegler
 * date : 2020/8/9 21:53
 */
public interface ZUnit {
    void test();
}
