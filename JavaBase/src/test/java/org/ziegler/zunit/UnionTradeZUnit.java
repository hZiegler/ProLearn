package org.ziegler.zunit;

public class UnionTradeZUnit extends ZUnitHumanObjBase {

    public UnionTradeZUnit(String methodName) {
        super(methodName);
    }

    @Test
    public void test1(HumanObject humanObj) {
        System.out.println("UnionTradeZUnit.test1");

        System.out.println("humanObj.toString() = " + humanObj.toString());
    }

    @Test
    private void test2(HumanObject humanObj) {
        System.out.println("UnionTradeZUnit.test2");
    }

    @Test
    void test3(HumanObject humanObj) {
        System.out.println("UnionTradeZUnit.test3");
    }

    void test4(HumanObject humanObject) {
        System.out.println("UnionTradeZUnit.test4");
    }

}
