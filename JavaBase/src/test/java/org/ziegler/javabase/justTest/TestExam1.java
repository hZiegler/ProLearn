package org.ziegler.javabase.justTest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import org.junit.jupiter.api.Test;

/**
 * 1. 平面上n个不重复点(x1,y1)，(x2,y2)……(xn,yn) ，找出其中两点间最短的距离，距离强转成整形返回。
 *
 * struct Point {
 *	int x;
 *	int y;
 * };
 *
 * @auther HanZhe
 * @date 2020/4/28 15:26
 */
public class TestExam1 {

    @Test
    void test() {

        List<Pair<Integer, Integer>> list = new ArrayList<>();

        list.add(ImmutablePair.of(2, 3));
        list.add(ImmutablePair.of(5, 8));
        list.add(ImmutablePair.of(8, 7));
        list.add(ImmutablePair.of(3, 2));
        list.add(ImmutablePair.of(9, 2));

        List<Triple<Pair<Integer, Integer>, Pair<Integer, Integer>, Integer>> operatorList = new ArrayList<>();

        /** 方法2 */
        for (int i = 0; i< list.size() - 2; ++i) {
            for (int j = i+1; j< list.size(); ++j) {
                Pair<Integer, Integer> iPair = list.get(i);
                Pair<Integer, Integer> jPair = list.get(j);

                int distance = computeDistance(iPair, jPair);
                operatorList.add(ImmutableTriple.of(iPair, jPair, distance));
            }
        }

//        System.out.println("operatorList = " + operatorList);

//        operatorList.forEach(triple -> {
//            System.out.println("triple = " + triple);
//        });

        operatorList.sort((aTriple, bTriple) -> Float.compare(aTriple.getRight(), bTriple.getRight()));

        operatorList.forEach(triple -> {
            System.out.println("triple = " + triple);
        });
    }

    private int computeDistance(Pair<Integer, Integer> aPair, Pair<Integer, Integer> bPair) {
        long total = (aPair.getLeft() - bPair.getLeft()) * (aPair.getLeft() - bPair.getLeft()) +
                (aPair.getRight() - bPair.getRight()) * (aPair.getRight() - bPair.getRight());

        double sqrt = Math.sqrt(total);
        return (int)sqrt;
    }
}
