package org.ziegler.javabase.justTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import org.junit.jupiter.api.Test;

/**
 * 1. 平面上n个不重复点(x1,y1)，(x2,y2)……(xn,yn) ，找出其中两点间最短的距离，距离强转成整形返回。
 * <p>
 * struct Point {
 * int x;
 * int y;
 * };
 *
 * @auther HanZhe
 * @date 2020/4/28 15:26
 */
public class TestExam {

    @Test
    void test() {

        List<Pair<Integer, Integer>> list = new ArrayList<>();

        list.add(ImmutablePair.of(2, 4));
        list.add(ImmutablePair.of(5, 8));
        list.add(ImmutablePair.of(8, 7));
        list.add(ImmutablePair.of(3, 2));
        list.add(ImmutablePair.of(9, 2));

        List<Triple<Pair<Integer, Integer>, Pair<Integer, Integer>, Integer>> operatorList = new ArrayList<>();

        /** 方法1： */
        List<Pair<Integer, Integer>> subList = list;

        do {
            Pair<Integer, Integer> aPair = subList.get(0);
            subList = subList.subList(1, subList.size());

            for (Pair<Integer, Integer> bPair : subList) {
                int distance = computeDistance(aPair, bPair);
                operatorList.add(ImmutableTriple.of(aPair, bPair, distance));
            }

        } while (subList.size() > 1);


        Optional<Integer> integer1 = Optional.of(5);
        Integer integer = integer1.orElseGet(RandomUtils::nextInt);

//        System.out.println("operatorList = " + operatorList);

//        operatorList.forEach(triple -> {
//            System.out.println("triple = " + triple);
//        });

        operatorList.sort((aTriple, bTriple) -> Float.compare(aTriple.getRight(), bTriple.getRight()));

        operatorList.forEach(triple -> System.out.println("triple = " + triple));
    }

    private int computeDistance(Pair<Integer, Integer> aPair, Pair<Integer, Integer> bPair) {
        long total = (aPair.getLeft() - bPair.getLeft()) * (aPair.getLeft() - bPair.getLeft()) +
                (aPair.getRight() - bPair.getRight()) * (aPair.getRight() - bPair.getRight());

        double sqrt = Math.sqrt(total);
        return (int) sqrt;
    }

    @Test
    void test333() {

        String smge = null;

        smge.equals("kkk");

        final List<String> game = List.of("game", "agme", "ss");

        List.of("type", "jakc");

        Map.of("key", "value");


        Set.of("key", "value", "mnae");

        new Thread(() -> {
            System.out.println("game = " + game);
        });
        String name = "jack";
        dddd(name);


    }

    private String testsssss() {
        return "dsfsdfsdfd";
    }


    private void dddd(String name) {

    }

}
