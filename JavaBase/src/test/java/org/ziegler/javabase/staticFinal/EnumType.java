package org.ziegler.javabase.staticFinal;

import java.util.Arrays;

public enum EnumType {
    A(ConstA.INDEX_A),
    B(ConstA.INDEX_B),
    C(ConstA.INDEX_C),
//    M(ConstA.INDEX_MAX),
    ;

    public static final int MAX_TIME_ID;

    static {
//        System.out.println("EnumType.static initializer");
//        System.out.println("EnumType static call ConstA.INDEX_MAX = " + ConstA.INDEX_MAX);

        MAX_TIME_ID = Arrays.stream(EnumType.values()).mapToInt(EnumType::getIndex).max()
                .orElseThrow(ExceptionInInitializerError::new);

//        System.out.println("MAX_TIME_ID = " + MAX_TIME_ID);
    }

    private final int index;

    EnumType(int index) {
        this.index = index;
//        System.out.println("index = " + index);
    }

    public int getIndex() {
        return index;
    }


}
