package org.ziegler.javabase.staticFinal;

public class ConstA {

    public static int INDEX_A = 111;
    public static final int INDEX_B = 222;
    public static final int INDEX_C = 333;
    public static final int INDEX_MAX = EnumType.MAX_TIME_ID + 1;

    static {
//        System.out.println("ConstA.static initializer");
//        System.out.println("INDEX_MAX = " + INDEX_MAX);
//        System.out.println("EnumType.MAX_TIME_ID = " + EnumType.MAX_TIME_ID);
    }

}
