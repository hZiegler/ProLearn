package org.ziegler.javabase.staticFinal;

public class Test {

    @org.junit.jupiter.api.Test
    void test1() {
        System.out.println("TestInit.test1");

        System.out.println("ConstA.INDEX_MAX = " + ConstA.INDEX_MAX);
        System.out.println("EnumType.MAX_TIME_ID = " + EnumType.MAX_TIME_ID);

        for (EnumType value : EnumType.values()) {
            System.out.println("value = " + value + ", index = " + value.getIndex());
        }
    }

    @org.junit.jupiter.api.Test
    void test2() {

        System.out.println("TestInit.test2");

        for (EnumType value : EnumType.values()) {
            System.out.println("value = " + value + ", index = " + value.getIndex());
        }

        System.out.println("ConstA.INDEX_MAX = " + ConstA.INDEX_MAX);
        System.out.println("EnumType.MAX_TIME_ID = " + EnumType.MAX_TIME_ID);
    }

    @org.junit.jupiter.api.Test
    void test3() {
        System.out.println("TestInit.test3");
        System.out.println("ConstA.INDEX_MAX = " + ConstA.INDEX_MAX);
        System.out.println("EnumType.MAX_TIME_ID = " + EnumType.MAX_TIME_ID);
    }

    @org.junit.jupiter.api.Test
    void test4() {
        System.out.println("TestInit.test4");
        System.out.println("EnumType.MAX_TIME_ID = " + EnumType.MAX_TIME_ID);
        System.out.println("ConstA.INDEX_MAX = " + ConstA.INDEX_MAX);
    }
}
