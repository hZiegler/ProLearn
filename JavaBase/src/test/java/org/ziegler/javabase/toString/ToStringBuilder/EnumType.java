package org.ziegler.javabase.toString.ToStringBuilder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import org.junit.jupiter.api.Test;

public enum EnumType {
    EnumA(1, "A"),
    EnumB(2, "B"),
    ;

    private final int index;
    private final String value;

    EnumType(int index, String value) {
        this.index = index;
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("index", index)
                .append("value", value)
                .toString();
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }

    @Test
    void test() {
        System.out.println("EnumA = " + EnumA);
        System.out.println("EnumB = " + EnumB);
    }
}
