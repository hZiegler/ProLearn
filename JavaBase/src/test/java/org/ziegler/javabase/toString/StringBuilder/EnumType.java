package org.ziegler.javabase.toString.StringBuilder;

public enum EnumType {
    EnumA(1, "A"),
    EnumB(2, "B"),
    ;

    private final int index;
    private final String value;

    EnumType(int index, String value) {
        this.index = index;
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EnumType{");
        sb.append("index=").append(index);
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }


}
