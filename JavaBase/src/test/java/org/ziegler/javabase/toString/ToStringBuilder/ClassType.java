package org.ziegler.javabase.toString.ToStringBuilder;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ClassType {
    private final int index;
    private final String name;
    private final int age;

    public ClassType(int index, String name, int age) {
        this.index = index;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("index", index)
                .append("name", name)
                .append("age", age)
                .toString();
    }


}
