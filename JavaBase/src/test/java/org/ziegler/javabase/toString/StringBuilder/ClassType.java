package org.ziegler.javabase.toString.StringBuilder;

public class ClassType {
    private final int index;
    private final String name;
    private final int age;

    public ClassType(int index, String name, int age) {
        this.index = index;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClassType{");
        sb.append("index=").append(index);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }


}
