package org.ziegler.javabase.toString.StringBuilder;

public class Test {

    @org.junit.jupiter.api.Test
    void test() {
        final ClassType jack = new ClassType(1, "jack", 32);
        System.out.println("jack = " + jack);
    }

    @org.junit.jupiter.api.Test
    void testEnum() {
        System.out.println("EnumType.EnumA = " + EnumType.EnumA);
        System.out.println("EnumType.EnumB = " + EnumType.EnumB);
    }
}
