package org.ziegler.javabase.learnEnum;

import java.util.EnumSet;
import java.util.Set;

public class TestEnumSet {

    enum Style { BOLD, ITALIC, UNDERLINE, STRIKETHROUGH }

    static void applyStyles(Set<Style> styles) {
        styles.forEach(System.out::println);

        if (styles.contains(Style.ITALIC)) {
            System.out.println("Style.ITALIC");
        }

    }

    @org.junit.jupiter.api.Test
    void testEnumSet() {
        applyStyles(EnumSet.of(Style.BOLD, Style.UNDERLINE, Style.ITALIC));
    }
}
