package org.ziegler.javabase.learnEnum;

import org.junit.jupiter.api.Test;

public class TestEnumMap {

    @Test
    void test1() {

        Phase.Transition.printMap();

        Phase.Transition transition = Phase.Transition.of(Phase.LIQUID, Phase.GAS);
        System.out.println("transition = " + transition);

        Phase.Transition transition2 = Phase.Transition.of2(Phase.LIQUID, Phase.GAS);
        System.out.println("transition2 = " + transition2);

    }
}
