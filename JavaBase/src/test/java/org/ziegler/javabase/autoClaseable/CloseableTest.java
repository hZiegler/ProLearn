package org.ziegler.javabase.autoClaseable;

import org.junit.jupiter.api.Test;

public class CloseableTest {

    @Test
    void test() {

        try (CloseableObjectA closeableObjectA = new CloseableObjectA();
             CloseableObjectB closeableObjectB = new CloseableObjectB()) {

            System.out.println("closeableObjectA.toString() = " + closeableObjectA.toString());
            System.out.println("closeableObjectB.toString() = " + closeableObjectB.toString());

            closeableObjectA.testException();
        } catch (Throwable throwable) {
            System.out.println("throwable:" + throwable.getMessage());
            for (Throwable st : throwable.getSuppressed()) {
                System.out.println("suppressed:" + st.getMessage());
            }
        } finally {
            System.out.println("try-with-resources finally");
        }
    }

    @Test
    void test2() {

        CloseableObjectA closeableObjectA = new CloseableObjectA();
        CloseableObjectB closeableObjectB = new CloseableObjectB();

        try {
            System.out.println("closeableObjectA.toString() = " + closeableObjectA.toString());
            System.out.println("closeableObjectB.toString() = " + closeableObjectB.toString());

            closeableObjectA.testException();
        } catch (Throwable throwable) {
            System.out.println("throwable:" + throwable.getMessage());
            for (Throwable st : throwable.getSuppressed()) {
                System.out.println("suppressed:" + st.getMessage());
            }
        } finally {
            System.out.println("try-finally");
            closeableObjectA.close();
            closeableObjectB.close();
        }
    }
}
