package org.ziegler.javabase.autoClaseable;

public class CloseableObjectA implements AutoCloseable {
    public CloseableObjectA() {
        System.out.println("CloseableObjectA");
    }

    @Override
    public void close() {
        System.out.println("CloseableObjectA.close");

        throw new RuntimeException("CloseableObjectA.close throw exception");
    }

    public void testException() {
        throw new RuntimeException("throwException test");
    }
}
