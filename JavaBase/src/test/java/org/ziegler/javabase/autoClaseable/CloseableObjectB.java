package org.ziegler.javabase.autoClaseable;

public class CloseableObjectB implements AutoCloseable {
    public CloseableObjectB() {
        System.out.println("CloseableObjectB");
    }

    @Override
    public void close() {
        System.out.println("CloseableObjectB.close");
        throw new RuntimeException("CloseableObjectB.close throw exception");
    }
}
