package org.ziegler.javabase;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

public class ComTest {

    @Test
    void test() {
        int min = Math.min(-3, 0);
        System.out.println("min = " + min);

        int max = Math.max(-3, 0);
        System.out.println("max = " + max);
    }

    private Shape completion() {
        Ellipse2D ellipse2D = new Ellipse2D.Double();
        Rectangle rectangle = new Rectangle();

        String string = "";

        List<String> strings = List.of();

        for (String str : strings) {
            if (!str.isEmpty()) {
                System.out.println("str = " + str);
            }
        }

        return ellipse2D;
    }

    public static final String data1 = "Hello";
    public static final String data2 = "world";
    public static final String data3 = "game";

    @Test
    Shape test2() {
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle2 = new Rectangle();

        List<Shape> shapes = List.of();

        List<Shape> shapes2 = List.of();

        for (Shape shape : shapes) {

        }

        Map<Integer, String> testMap = Map.of();
        testMap.put(1, "one");

        Set<Long> testSet = Set.of();

//        List<Integer> integers


        return rectangle;
    }

    @Test
    void testRandom() {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        current.nextBoolean();
    }


    /**
     *
     * @param m
     * @return
     * @throws RuntimeException
     */
    @Test
    int testException(int m) {

        int n = ThreadLocalRandom.current().nextInt();
        if (n > 100) {
            throw new RuntimeException("test");
        }

        return n;
    }

    @Test
    void testReduce() {

        List.of(1, 2, 3).stream().reduce(0, (a, b) -> a + b);
    }

    interface T {

    }

    interface P {

    }

    class A implements T {
        @Override
        public String toString() {
            return "A";
        }
    }

    class B extends A {
        @Override
        public String toString() {
            return "B";
        }
    }

    @Test
    void testComparator() {
        T t = new B();

        B t1 = (B) t;
        System.out.println("t1 = " + t1);

        B b = (B & P)t;
        System.out.println("b = " + b);



    }

    @Test
    void testIntStream() {
//        IntStream.range
//
//        LongStream.of(3, 2).map
    }

    @Test
    void testJson() {
        String str = "ABC def 100";

    }

    String[] str = new String[2];

    @Test
    void testStringArray() {

        String[] aa = {"", ""};

        aa[0] = "ddd";
        aa[1] = "bbb";

        final String s1 = "";
        final String s2 = "";

        for(int i=0; i<2; ++i) {
            if (i == 2) {
                str[i] = s2;
            } else if (i == 1) {
                str[i] = s1;
            }
        }

        System.out.println("str = " + str);
    }

    @Test
    void testFinalLambda() {

        int n = 9;

        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(() -> {
            System.out.println("n = " + n);
        });

    }
}
