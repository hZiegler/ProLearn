package org.ziegler.javabase.tryCatch;

import org.junit.jupiter.api.Test;

public class TestTry {

    int checkReturn() {
        int x = 0;
        try {
            return ++x;
        } finally {
            return ++x;
        }
    }

    int checkReturn2() {
        int x = 0;
        try {
            return ++x;
        } finally {
            ++x;
        }
    }

    private int x = 0;
    int checkReturn3() {
        try {
            return ++x;
        } finally {
            ++x;
        }
    }

    @Test
    void test1() {
        final int i = checkReturn();
        System.out.println("i = " + i);
    }

    @Test
    void test2() {
        final int i2 = checkReturn2();
        System.out.println("i2 = " + i2);
    }

    @Test
    void test3() {
        final int i3 = checkReturn3();
        System.out.println("i3 = " + i3);
    }
}
