package org.ziegler.javabase.tryCatch;

import org.junit.jupiter.api.Test;

public class TestTry2 {

    int checkReturn() {
        int x = 0;
        try {
            return ++x;
        } finally {
            ++x;
            return x;
        }
    }

    @Test
    void test1() {
        final int i = checkReturn();
        System.out.println("i = " + i);
    }

}
