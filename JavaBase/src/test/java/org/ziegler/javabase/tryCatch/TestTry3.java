package org.ziegler.javabase.tryCatch;

import org.junit.jupiter.api.Test;

public class TestTry3 {

    private int x = 0;
    int checkReturn() {
        try {
            System.out.println("TestTry3.checkReturn try");
            return inc();
        } finally {
            System.out.println("TestTry3 finally before inc");
            inc();
            System.out.println("TestTry3 finally after inc");
        }
    }

    int inc() {
        System.out.println("TestTry3.inc");
        return ++x;
    }

    @Test
    void test1() {
        final int i = checkReturn();
        System.out.println("i = " + i);
    }

}
