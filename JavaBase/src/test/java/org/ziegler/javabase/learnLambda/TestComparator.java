package org.ziegler.javabase.learnLambda;

import java.util.Comparator;

import org.junit.jupiter.api.Test;

public class TestComparator {
    @Test
    void testComparing() {


        final Comparator<Person> personComparator = Comparator.comparing(Person::getName)
                .reversed()
                .thenComparing(Person::getId)
                .thenComparing(Person::getAge);

        Comparator.comparing(Person::getName, String::compareTo);


    }

    class Person {
        int id;
        String name;
        int age;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
