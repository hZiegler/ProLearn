package org.ziegler.javabase.learnLambda;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

public class TestLambda {
    @Test
    void test1() {
        Function<Integer, String> func1 = n -> String.valueOf(n);
        Function<String, Integer> func2 = s -> Integer.valueOf(s) * 3;
        Function<Integer, Integer> func3 = n -> n * n;

        Function<Integer, Integer> integerIntegerFunction = func1.andThen(func2).andThen(func3);
        Integer apply = integerIntegerFunction.apply(5);
        System.out.println("apply = " + apply);

    }

    @Test
    void testPredict() {
        Predicate<Integer> pred1 = n -> n > 50;
        Predicate<Integer> pred2 = n -> n % 2 == 0;
        Predicate<Integer> pred3 = n -> n % 5 == 0;

        Predicate<Integer> and = pred1.and(pred2).and(pred3);
        {
            boolean test = and.test(77);
            System.out.println("test = " + test);
        }
        {
            boolean test = and.test(80);
            System.out.println("test = " + test);
        }

        {
            boolean test = and.test(40);
            System.out.println("test = " + test);
        }

    }
}
