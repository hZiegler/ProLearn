package org.ziegler.javabase.intelliJ.learn.demo.domain;

public interface Car {

    void drive();

    int getNumberOfDoors();
}
