package org.ziegler.javabase.intelliJ.learn.demo.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CarImpl implements Car {
    private int numberOfDoors;

    private Fuel fuel;
    private final String description;

    private CarDelegate delegate;

    public CarImpl(int numberOfDoors, String description) {
        this.numberOfDoors = numberOfDoors;
        this.description = description;
    }

    public CarImpl(int numberOfDoors, Fuel fuel, String description) {
        this.numberOfDoors = numberOfDoors;
        this.fuel = fuel;
        this.description = description;
    }

    @Override
    public void drive() {
        System.out.println("I like driving in my car.");
    }

    @Override
    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("numberOfDoors", numberOfDoors)
                .append("fuel", fuel)
                .append("description", description)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarImpl car = (CarImpl) o;

        if (fuel != car.fuel) return false;
        return description.equals(car.description);
    }

    @Override
    public int hashCode() {
        int result = fuel.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }
}
