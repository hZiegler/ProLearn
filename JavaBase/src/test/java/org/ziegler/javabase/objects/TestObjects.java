package org.ziegler.javabase.objects;

import java.util.Objects;

import org.junit.jupiter.api.Test;

public class TestObjects {


    @Test
    void name() {
        Integer p = null;
        Objects.requireNonNull(p);
    }

    @Test
    void test1() {
        Integer p = null;
        Objects.requireNonNull(p, "p can't be null");
    }

    @Test
    void t1() {
        long n = -111;
        int m = (int)n;
        System.out.println("m = " + m);
    }

    @Test
    void t2() {
        int n = 65534;
        System.out.println("n = " + n);
        short m = (short) n;
        System.out.println("m = " + m);
    }
}
