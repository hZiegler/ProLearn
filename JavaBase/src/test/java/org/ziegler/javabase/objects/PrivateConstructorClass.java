package org.ziegler.javabase.objects;

public class PrivateConstructorClass {

    private PrivateConstructorClass() {
    }

    public static PrivateConstructorClass of() {
        return new PrivateConstructorClass();
    }

    public static PrivateConstructorClass ofExtend() {
        return new ExtendClass();
    }

    private static class ExtendClass extends PrivateConstructorClass {
        private ExtendClass() {

        }
    }

}
