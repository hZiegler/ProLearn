package org.ziegler.javabase.learnInterface;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

public class TestInterface {

    @Test
    void testLambda() {
        test(n -> n > 3);
    }

    private void test(Predicate<Integer> function) {
        final boolean test = function.test(3);
        System.out.println("test = " + test);
    }
}
