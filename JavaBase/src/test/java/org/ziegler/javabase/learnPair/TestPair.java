package org.ziegler.javabase.learnPair;

import org.apache.commons.lang3.tuple.Pair;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPair {

    @Test
    void test() {

        Pair<Integer, Integer> pair1 = Pair.of(1, 0);

        Pair<String, String> pair2 = Pair.of("jack", "rose");

        if (!pair1.equals(pair2)) {
            System.out.println("not equals");
        } else {
            System.out.println("equals");
        }

        Assertions.assertAll();
    }
}
