package org.ziegler.javabase.thread.shutdown;

import org.ziegler.javabase.thread.DelayUtil;

public class ShutdownTest1 {
    public static void main(String[] args) {
        System.out.println("ShutdownHootTest.testHook");
        final Thread thread = new Thread(() -> {
            // 延迟1秒
            DelayUtil.second(1);
            System.out.println("ShutdownTest1.main finish");
        });
        thread.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // 调用
            System.out.println("run addShutdownHook");
        }));
    }
}
