package org.ziegler.javabase.thread;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TestFieldConstruct extends FieldConstructBase {

    private int id = 5;

    private String name = "jack";

    private String[] strArr = new String[2];

    public TestFieldConstruct() {
        super();
    }

    @Override
    void init() {
        name = "ttt";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("strArr", strArr)
                .toString();
    }

    public static void main(String[] args) {
        TestFieldConstruct t = new TestFieldConstruct();
        System.out.println("t = " + t);
    }
}
