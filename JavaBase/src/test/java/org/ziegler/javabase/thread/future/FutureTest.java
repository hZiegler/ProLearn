package org.ziegler.javabase.thread.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FutureTest {

    static ExecutorService executorService = Executors.newCachedThreadPool();

    @AfterEach
    void tearDown() {
       executorService.shutdown();
    }

    void delay(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testFuture() throws ExecutionException, InterruptedException {
        final Future<Long> future = executorService.submit(() -> {
            long result = 1;

            for (int i = 0; i < 5; i++) {
                result = result * ThreadLocalRandom.current().nextInt(1, 10);
            }

            return result;
        });

        final long value = future.get();
        System.out.println("result = " + value);
    }

    @Test
    void testCompletableFuture() {
        CompletableFuture<Long> completableFuture = new CompletableFuture<>();

    }

    /**
     * get 出现TimeoutException，并不会中断或者取消运算线程
     * @throws InterruptedException
     */
    @Test
    void testGetTimeoutException() throws InterruptedException {
        final Future<Long> future = executorService.submit(() -> {
            System.out.println("start delay 3000");
            Thread.sleep(3000);
            System.out.println("after delay 3000");
            return 888L;
        });

        try {
            final long value = future.get(1, TimeUnit.SECONDS);
            System.out.println("value = " + value);
            Assertions.fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            // get timeout
            System.out.println("get timeout and cancel task");
            future.cancel(true);
        }

        executorService.shutdown();
        executorService.awaitTermination(100, TimeUnit.SECONDS);

        System.out.println("wait shutdown");
    }
}