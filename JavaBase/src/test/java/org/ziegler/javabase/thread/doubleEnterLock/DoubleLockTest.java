package org.ziegler.javabase.thread.doubleEnterLock;

import org.junit.jupiter.api.Test;

public class DoubleLockTest {
    @Test
    void testDoubleLock() {
        LoggingWidget loggingWidget = new LoggingWidget();
        loggingWidget.doSomething();

        loggingWidget.doOtherThing();
    }
}
