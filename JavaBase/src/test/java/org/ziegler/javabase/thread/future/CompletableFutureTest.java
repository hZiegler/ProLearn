package org.ziegler.javabase.thread.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.time.StopWatch;

import org.junit.jupiter.api.Test;

public class CompletableFutureTest {

    void delay(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSupplyAsync() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return 888;
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final Integer value = completableFuture.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }


    @Test
    void testThenApply() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);

            return 888;
        });

        final CompletableFuture<String> thenApplyFuture = completableFuture.thenApply(
                result -> "result=" + result);

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final String value = thenApplyFuture.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenCompose() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<String> thenCompose = completableFuture.thenCompose(value ->
                CompletableFuture.supplyAsync(() -> {
                    delay(1000);
                    System.out.println("thenCompose future threadId = " + Thread.currentThread().getId());
                    return "String value = " + value * 10;
                }));

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final String value = thenCompose.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenComposeAsync() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<String> thenCompose = completableFuture.thenComposeAsync(value ->
                CompletableFuture.supplyAsync(() -> {
                    delay(1000);
                    System.out.println("thenComposeAsync future threadId = " + Thread.currentThread().getId());
                    return "String value = " + value * 10;
                }));

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final String value = thenCompose.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenCombine() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<String> thenCombine = completableFuture.thenCombine(CompletableFuture.supplyAsync(() -> {
                    delay(1000);
                    System.out.println("second future threadId = " + Thread.currentThread().getId());
                    return "value=" + 999;
                }), (a, b) -> {
                    System.out.println("thenCombine future threadId = " + Thread.currentThread().getId());
                    return a + " join " + b;
                });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final String value = thenCombine.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenCombineAsync() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<String> thenCombine = completableFuture.thenCombineAsync(CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("second future threadId = " + Thread.currentThread().getId());
            return 999L;
        }), (a, b) -> {
            System.out.println("thenCombineAsync future threadId = " + Thread.currentThread().getId());
            return "value=" + (a * b);
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        final String value = thenCombine.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenAccept() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        completableFuture.thenAccept(value -> {
            // thenAccept
            System.out.println("thenAccept value = " + value);
            System.out.println("thenAccept time:" + stopWatch.getTime());
            System.out.println("thenAccept threadId = " + Thread.currentThread().getId());
        });

        final int value = completableFuture.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void testThenAcceptAsync() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        completableFuture.thenAcceptAsync(value -> {
            // thenAccept
            System.out.println("thenAccept value = " + value);
            System.out.println("thenAccept time:" + stopWatch.getTime());
            System.out.println("thenAccept threadId = " + Thread.currentThread().getId());
        });

        final int value = completableFuture.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

    @Test
    void allOf() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 999;
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        CompletableFuture.allOf(completableFuture, completableFuture2).join();
        System.out.println("allOf time:" + stopWatch.getTime());

        final int value = completableFuture.get();
        System.out.println("value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());

        final int value2 = completableFuture2.get();
        System.out.println("value2 = " + value2);
        System.out.println("get value2 time:" + stopWatch.getTime());
    }

    @Test
    void anyOf() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 888;
        });

        final CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            System.out.println("first future threadId = " + Thread.currentThread().getId());
            return 999;
        });

        StopWatch stopWatch = StopWatch.createStarted();
        System.out.println("start:" + stopWatch.getTime());

        // 返回一个新的CompletableFuture，可能执行完成任意一个结果
        final CompletableFuture<Object> anyCompleteFuture = CompletableFuture
                .anyOf(completableFuture, completableFuture2);

        System.out.println("anyOf time:" + stopWatch.getTime());

        final int value = (int)anyCompleteFuture.get();
        System.out.println("any value = " + value);
        System.out.println("get value time:" + stopWatch.getTime());
    }

}
