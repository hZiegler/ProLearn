package org.ziegler.javabase.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;

public class testMultiThread {

    private static StopWatch watch = StopWatch.createStarted();

    private static ExecutorService exec = Executors.newFixedThreadPool(4);

    public static void main(String[] args) throws InterruptedException {
        Runnable t = () -> {
            //                Thread.sleep(1000);
            StopWatch watch2 = StopWatch.createStarted();
            Float f = 9.0f;
            for (int i=0;i<1000000000; ++i) {
                f = f + 1;
            }
            System.out.println("f:" + f + ",time:" + watch2.getTime());
        };

        for (int i=0; i<40; ++i) {
            exec.execute(t);
        }

        System.out.println("start:" + watch.getTime());

        exec.shutdown();
        exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

        // 打印结束时间
        System.out.println("finish time:" + watch.getTime());

//        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
//            // 打印结束时间
//            System.out.println("finish time:" + watch.getTime());
//        }));
    }
}
