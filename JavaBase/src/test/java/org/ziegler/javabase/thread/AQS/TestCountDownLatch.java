package org.ziegler.javabase.thread.AQS;

import java.util.concurrent.CountDownLatch;

import org.junit.jupiter.api.Test;

public class TestCountDownLatch {

    @Test
    void test() {

        CountDownLatch countDownLatch = new CountDownLatch(1);

        new Thread(() -> {
            countDownLatch.countDown();
        }).start();

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("TestCountDownLatch.test");

    }
}
