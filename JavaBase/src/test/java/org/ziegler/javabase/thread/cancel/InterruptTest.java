package org.ziegler.javabase.thread.cancel;

import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang3.time.StopWatch;

import org.junit.jupiter.api.Test;
import org.ziegler.javabase.thread.DelayUtil;

public class InterruptTest {


    @Deprecated
    @Test
    void testInterrupt() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        Thread thread = new Thread(() -> {
            countDownLatch.countDown();

            System.out.println("while start");

            final StopWatch started = StopWatch.createStarted();
            while (true) {
                if (started.getTime() > 2000) {
                    System.out.println("while finish");
                    break;
                }
            }

            System.out.println("Thread finish");
        });

        thread.start();

        countDownLatch.await();

        DelayUtil.second(1);

        System.out.println("delay 1 second and interrupt");
        thread.interrupt();
        System.out.println("after interrupt");

        DelayUtil.second(2);
    }

    @Test
    void testInterrupt2() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        Thread thread = new Thread(() -> {
            countDownLatch.countDown();

            System.out.println("while start");

            try {
                Thread.sleep(2000);
                System.out.println("sleep finish");

            } catch (InterruptedException e) {
                System.out.println("线程中断了");
                Thread.currentThread().interrupt();
            }

            System.out.println("Thread finish");
        });

        thread.start();

        countDownLatch.await();

        DelayUtil.second(1);

        System.out.println("delay 1 second and interrupt");
        thread.interrupt();
        System.out.println("after interrupt");

        DelayUtil.second(2);
    }

    @Test
    void testInterrupt3() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        Thread thread = new Thread(() -> {
            countDownLatch.countDown();

            System.out.println("while start");

            final StopWatch started = StopWatch.createStarted();
            while (!Thread.currentThread().isInterrupted()) {
                if (started.getTime() > 2000) {
                    System.out.println("while finish");
                    break;
                }
            }

            System.out.println("Thread finish");
        });

        thread.start();

        countDownLatch.await();

        DelayUtil.second(1);

        System.out.println("delay 1 second and interrupt");
        thread.interrupt();
        System.out.println("after interrupt");

        DelayUtil.second(2);
    }

}
