package org.ziegler.javabase.thread.future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.jupiter.api.Test;

class ShopTest {
    @Test
    void testGetPriceAsyncComplete() {
        Shop shop = new Shop();
        final Future<Double> futurePrice = shop.getPriceAsync("my favorate product");
        try {
            final Double price = futurePrice.get();
            System.out.println("price = " + price);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testGetPriceAsyncCompleteExceptionally() {
        Shop shop = new Shop();
        final Future<Double> futurePrice = shop.getPriceAsync("");
        try {
            final Double price = futurePrice.get();
            System.out.println("price = " + price);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


    @Test
    void getPriceAsync2() {
        Shop shop = new Shop();
        final Future<Double> futurePrice = shop.getPriceAsync("my favorate product");
        try {
            final Double price = futurePrice.get();
            System.out.println("price = " + price);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getPriceAsync2Exceptionally() {
        Shop shop = new Shop();
        final Future<Double> futurePrice = shop.getPriceAsync("");
        try {
            final Double price = futurePrice.get();
            System.out.println("price = " + price);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}