package org.ziegler.javabase.thread;

import org.apache.commons.lang3.time.DateUtils;

import org.junit.jupiter.api.Assertions;

import static java.lang.Thread.sleep;

public class TestThread {


//    Thread t = new Thread() {
//
////        CountDownLatch latch = new CountDownLatch(1);
//
//        @Override
//        public void run() {
//            super.run();
//
//            try {
//                System.out.println("TestThread.run before");
//                //wait();
//
////                latch.wait();
//
//
////                this.wait(2 * DateUtils.MILLIS_PER_SECOND);
//                sleep(100 * DateUtils.MILLIS_PER_SECOND);
//
//                System.out.println("TestThread.run after");
//            } catch (IllegalMonitorStateException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    };


    public static void main(String[] args) {

        Assertions.assertFalse(null instanceof Thread);

        Thread th = new Thread(() -> {
            try {
                System.out.println("TestThread.run before");
                //wait();

//                latch.wait();

//                this.wait(2 * DateUtils.MILLIS_PER_SECOND);
                sleep(100 * DateUtils.MILLIS_PER_SECOND);

                System.out.println("TestThread.run after");
            } catch (IllegalMonitorStateException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Thread.currentThread().isInterrupted() = "
                        + Thread.currentThread().isInterrupted());
                Thread.currentThread().interrupt();
                System.out.println("Thread.currentThread().isInterrupted() = "
                        + Thread.currentThread().isInterrupted());
                System.out.println("Thread.interrupted() = " + Thread.interrupted());
//                System.out.println("Thread.interrupted() = " + Thread.interrupted());
                System.out.println("Thread.currentThread().isInterrupted() = "
                        + Thread.currentThread().isInterrupted());
            }

        });
        th.start();

        delay(1);

//        Runtime.getRuntime().halt(1);
//        System.exit(0);

        th.interrupt();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // 关闭钩子
            System.out.println("addShutdownHook1");
        }));

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // 关闭钩子
            System.out.println("addShutdownHook2");
        }));
    }

    public static final void delay(long second) {
        try {
            sleep(second * DateUtils.MILLIS_PER_SECOND);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
