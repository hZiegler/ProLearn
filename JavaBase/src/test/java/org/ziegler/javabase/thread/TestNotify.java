package org.ziegler.javabase.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestNotify {

//    private Object lock = new Object();

    private ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) throws InterruptedException {

        TestNotify testNotify = new TestNotify();
        testNotify.run();
        testNotify.run();

        System.out.println("before TestNotify.main");

        synchronized (testNotify) {
            testNotify.notify();
        }

        System.out.println("TestNotify.main notify");

        Thread.sleep(1500);

        synchronized (testNotify) {
            testNotify.notify();
        }

        System.out.println("TestNotify.main notify");

        Thread.sleep(1000);

        synchronized (testNotify) {
            testNotify.notifyAll();
        }

        System.out.println("TestNotify.main notifyAll");

        testNotify.shutdownGracefully();
    }

    private void run() {
        executorService.execute(() -> {

            long threadId = Thread.currentThread().getId();

            System.out.println("[" + threadId + "]before run sleep");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("[" + threadId + "]before TestNotify.run");

            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("[" + threadId + "]TestNotify.run");
        });
    }

    public void shutdownGracefully() throws InterruptedException {
        executorService.shutdown();
        executorService.awaitTermination(3, TimeUnit.SECONDS);
    }

}
