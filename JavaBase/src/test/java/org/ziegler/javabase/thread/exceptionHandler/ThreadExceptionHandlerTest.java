package org.ziegler.javabase.thread.exceptionHandler;

import org.junit.jupiter.api.Test;

public class ThreadExceptionHandlerTest {

    Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = (t, e) -> {
        System.out.println("thread id = " + t.getId()
                + ", defaultUncaughtExceptionHandler:" + e.toString());
    };

    Thread.UncaughtExceptionHandler uncaughtExceptionHandler = (t, e) -> {
        System.out.println("thread id = " + t.getId()
                + ", uncaughtExceptionHandler:" + e.toString());
    };

    @Test
    void testUncaughtExceptionHandler() throws InterruptedException {

        final Thread thread = new Thread(() -> {
            throw new RuntimeException("thread run");
        });

        thread.start();
        thread.join();

        Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);

        final Thread thread1 = new Thread(() -> {
            throw new RuntimeException("thread1 run");
        });

        final Thread thread2 = new Thread(() -> {
            throw new RuntimeException("thread2 run");
        });

        thread2.setUncaughtExceptionHandler(uncaughtExceptionHandler);

        thread1.start();

        thread2.start();
        thread1.join();
        thread2.join();
    }
}
