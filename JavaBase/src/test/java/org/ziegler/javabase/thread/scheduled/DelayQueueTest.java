package org.ziegler.javabase.thread.scheduled;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;

import org.apache.commons.lang3.time.StopWatch;

import org.junit.jupiter.api.Test;
import org.ziegler.javabase.thread.DelayUtil;

public class DelayQueueTest {

    // TODO test DelayQueue
    @Test
    void testDelayQueue() throws InterruptedException {
        DelayQueue delayQueue = new DelayQueue();
        delayQueue.add(new Message(1, 100));
        delayQueue.add(new Message(2, 600));
        delayQueue.add(new Message(3, 300));
        delayQueue.add(new Message(4, 400));
        delayQueue.add(new Message(5, 200));
        delayQueue.add(new Message(6, 500));

        StopWatch stopWatch = StopWatch.createStarted();

        DelayUtil.second(1);

        for (int i = 0; i < 6; i++) {
            Delayed delayed = delayQueue.take();
            System.out.println("delayed = " + delayed + ",time=" + stopWatch.getTime());
        }
    }
}
