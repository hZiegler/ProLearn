package org.ziegler.javabase.thread.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.StopWatch;

import org.junit.jupiter.api.Test;
import org.ziegler.javabase.thread.DelayUtil;

public class ExecutorServiceTest {

    static ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * ExecutorService.invokeAll 会阻塞等待所有任务执行完成
     */
    @Test
    void testInvokeAll() {
        Callable<Integer> callable1 = () -> {
            DelayUtil.millis(500);
            return 888;
        };

        Callable<Integer> callable2 = () -> {
            DelayUtil.millis(1000);
            return 999;
        };

        Callable<Integer> callable3 = () -> {
            DelayUtil.millis(1500);
            return 1111;
        };

        List<Callable<Integer>> callableList = new ArrayList<>();
        callableList.add(callable1);
        callableList.add(callable2);
        callableList.add(callable3);

        StopWatch stopWatch = StopWatch.createStarted();

        try {
            System.out.println("before invokeAll = " + stopWatch.getTime());
            final List<Future<Integer>> futures = executorService.invokeAll(callableList);
            System.out.println("after invokeAll = " + stopWatch.getTime());

            for (Future<Integer> future : futures) {
                try {
                    final Integer value = future.get();
                    System.out.println("value = " + value);
                    System.out.println("future.get = " + stopWatch.getTime());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * ExecutorService.invokeAll
     */
    @Test
    void testInvokeAllTimeout() {
        Callable<Integer> callable1 = () -> {
            DelayUtil.millis(500);
            return 777;
        };

        Callable<Integer> callable2 = () -> {
            DelayUtil.millis(900);
            return 888;
        };

        Callable<Integer> callable3 = () -> {
            DelayUtil.millis(1500);
            return 999;
        };

        List<Callable<Integer>> callableList = new ArrayList<>();
        callableList.add(callable1);
        callableList.add(callable2);
        callableList.add(callable3);

        StopWatch stopWatch = StopWatch.createStarted();

        try {
            System.out.println("before invokeAll time = " + stopWatch.getTime());
            final List<Future<Integer>> futures = executorService.invokeAll(callableList, 1, TimeUnit.SECONDS);
            System.out.println("after invokeAll time = " + stopWatch.getTime());

            for (Future<Integer> future : futures) {
                if (future.isCancelled()) {
                    System.out.println("future被取消");
                    continue;
                }

                try {
                    final Integer value = future.get();
                    System.out.println("value = " + value);
                    System.out.println("future.get time = " + stopWatch.getTime());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testExecutorCompletionService() throws InterruptedException, ExecutionException {
        ExecutorCompletionService<Integer> executorCompletionService
                = new ExecutorCompletionService<>(executorService);

        final StopWatch stopWatch = StopWatch.createStarted();

        System.out.println("start time = " + stopWatch.getTime());

        for (int i = 3; i > 0; i--) {
            final int n = i;
            executorCompletionService.submit(() -> {
                DelayUtil.second(n);
                return 111 * n;
            });

            System.out.println("submit time = " + stopWatch.getTime());
        }

        for (int i = 0; i < 3; i++) {
            final int returnValue = executorCompletionService.take().get();
            System.out.println("returnValue = " + returnValue);

            System.out.println("submit time = " + stopWatch.getTime());
        }


    }

    @Test
    void testCompletionService() {
        ExecutorCompletionService<Integer> executorCompletionService
                = new ExecutorCompletionService<>(executorService);

        for (int i = 3; i > 0; i--) {
            final int n = i;
            executorCompletionService.submit(() -> {
                DelayUtil.second(n);
                return 111 * n;
            });
        }

        for (int i = 0; i < 3; i++) {
            try {
                final Future<Integer> f = executorCompletionService.take();
                final Integer value = f.get();
                System.out.println("value = " + value);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }


    @Test
    void testSingleRestart() throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("run i:" + i);
                DelayUtil.second(1);
                if (i == 2) {
                    throw new NullPointerException("test");
                }
            }
        });

        executor.execute(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("run i:" + i);
                DelayUtil.second(1);
                if (i == 2) {
                    throw new NullPointerException("test");
                }
            }
        });

        System.out.println("wait");
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        System.out.println("after wait");

    }

    @Test
    void testSingleThreadPool() throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<?> future = executor.submit(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("run i:" + i);
                DelayUtil.second(1);
                if (i == 2) {
                    throw new NullPointerException("test");
                }
            }
        });

        try {
            future.get();
        } catch (ExecutionException e) {
            executor.submit(() -> {
                for (int i = 0; i < 5; i++) {
                    System.out.println("2run i:" + i);
                    DelayUtil.second(1);
                }
            });
        }

        executor.submit(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("run i:" + i);
                DelayUtil.second(1);
                if (i == 2) {
                    throw new NullPointerException("test");
                }
            }
        });

        System.out.println("wait");
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        System.out.println("after wait");
    }

    @Test
    void singleExecuteAll() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        List<Callable<Void>> listCallable = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            listCallable.add(() -> {
                DelayUtil.second(1);
                System.out.println("delay 1s");
                return null;
            });
        }

        try {
            executorService.invokeAll(listCallable);
            System.out.println("after invokeAll");
        } catch (InterruptedException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
            e.printStackTrace();
        }

        System.out.println("ExecutorServiceTest.singleExecuteAll");
        executorService.shutdown();
    }

    @Test
    void multiExecuteAll() {
        ExecutorService executorService = Executors.newCachedThreadPool();

        List<Callable<Void>> listCallable = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            listCallable.add(() -> {
                DelayUtil.second(1);
                System.out.println("delay 1s");
                return null;
            });
        }

        try {
            executorService.invokeAll(listCallable);
            System.out.println("after invokeAll");
        } catch (InterruptedException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
            e.printStackTrace();
        }

        System.out.println("ExecutorServiceTest.singleExecuteAll");
        executorService.shutdown();
    }

    @Test
    void fixedExecuteAll() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        List<Callable<Void>> listCallable = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            listCallable.add(() -> {
                DelayUtil.second(1);
                System.out.println("delay 1s");
                return null;
            });
        }

        try {
            executorService.invokeAll(listCallable);
            System.out.println("after invokeAll");
        } catch (InterruptedException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
            e.printStackTrace();
        }

        System.out.println("ExecutorServiceTest.singleExecuteAll");
        executorService.shutdown();
    }


    @Test
    void random6or7() {
        int n = RandomUtils.nextInt() % 2 + 6;
        System.out.println("我那天来？ = " + n);
    }
}
