package org.ziegler.javabase.throwable;

public class TestTryCatch {


    public static void main(String[] args) {
        try {
            System.out.println("TestTryCatch.main");
            throw new IllegalArgumentException("ooo");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException = " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Exception = " + e.getMessage());
        } finally {
            System.out.println("finally");
        }
    }

}
