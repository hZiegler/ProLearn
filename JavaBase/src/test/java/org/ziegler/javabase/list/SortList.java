package org.ziegler.javabase.list;

import java.util.ArrayList;
import java.util.List;

public class SortList {

    List<Integer> list = new ArrayList<>();

    public void add(Integer newValue) {
        boolean bInsert = false;
        for (int i = 0; i < list.size(); i++) {
            final Integer value = list.get(i);
            if (newValue >= value) {
                bInsert = true;
                list.add(i, newValue);
                break;
            }
        }

        if (!bInsert) {
            // 插入最后
            list.add(newValue);
        }

        if (list.size() >= 5) {
            list.remove(5 - 1);
        }
    }
    
    void printList() {
        list.forEach(v -> System.out.println("v = " + v));
    }

}
