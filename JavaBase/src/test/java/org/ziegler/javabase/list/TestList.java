package org.ziegler.javabase.list;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TestList {
    @Test
    void name() {
        List<Integer> list = new ArrayList<>();
        list.add(0, 888);
        list.add(0, 881);
        list.add(0, 882);
        list.add(0, 883);
        list.add(0, 884);

        for (Integer integer : list) {
            System.out.println("integer = " + integer);
        }
    }

    @Test
    void testLong() {
        System.out.println("Long.MAX_VALUE = " + Long.MAX_VALUE);
        final long l = Long.MAX_VALUE / (20 * 10000);
        System.out.println("l = " + l);
    }
}
