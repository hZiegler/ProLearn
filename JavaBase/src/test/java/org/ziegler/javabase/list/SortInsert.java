package org.ziegler.javabase.list;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class SortInsert {






    @Test
    void name() {


        SortList sortList = new SortList();
        sortList.add(3);
        sortList.add(9);
        sortList.add(7);
        sortList.add(1);
        sortList.add(2);
        sortList.add(6);

        sortList.printList();

    }

    @Test
    void testRemoveIndex() {
        List<Integer> list = new ArrayList<>(List.of(1, 5, 8, 7, 3, 9, 6));

        list.forEach(n -> System.out.println("n = " + n));

        for (int i = 3; i < list.size(); ++i) {
            list.remove(i);
        }

        System.out.println("after remove");
        list.forEach(n -> System.out.println("n = " + n));

    }

    @Test
    void testSubListAndRemoveAll() {
        List<Integer> list = new ArrayList<>(List.of(1, 5, 8, 7, 3, 9, 6));
        final List<Integer> removes = list.subList(4, list.size());
        list.removeAll(removes);

        list.forEach(System.out::println);
    }
}
