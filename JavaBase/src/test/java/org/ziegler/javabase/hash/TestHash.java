package org.ziegler.javabase.hash;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class TestHash {
    private final String name;
    private final int value;
    private final long time;

    public TestHash(String name, int value, long time) {
        this.name = name;
        this.value = value;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TestHash testHash = (TestHash) o;

        return new EqualsBuilder()
                .append(value, testHash.value)
                .append(time, testHash.time)
                .append(name, testHash.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(value)
                .append(time)
                .toHashCode();
    }
}
