package org.ziegler.javabase.hash;

import java.util.Objects;

public class TestHash2 {
    private final String name;
    private final int value;
    private final long time;

    public TestHash2(String name, int value, long time) {
        this.name = Objects.requireNonNull(name);
        this.value = value;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestHash2 testHash2 = (TestHash2) o;

        if (value != testHash2.value) return false;
        if (time != testHash2.time) return false;
        return name.equals(testHash2.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value;
        result = 31 * result + (int) (time ^ (time >>> 32));
        return result;
    }
}
