package org.ziegler.javabase.equals;

import java.awt.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EqualsTest {

    @Test
    void test1() {
        ColorPoint colorPoint = new ColorPoint(1, 2, Color.BLACK);
        Point point = new Point(1, 2);

        Assertions.assertFalse(colorPoint.equals(point));
        Assertions.assertTrue(point.equals(colorPoint));
    }
}
