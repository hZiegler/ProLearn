package org.ziegler.javabase;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class learnCompare {
    @Test
    void test() {

        Map<Long, String> testMap = new HashMap<>();
        testMap.put(7L, "7");
        testMap.put(100L, "100");

        int n = 7;

        String s = testMap.get(n);
        System.out.println("s = " + s);

        long l = 100;
        String s2 = testMap.get(l);
        System.out.println("s2 = " + s2);
    }
}
