package org.ziegler.javabase.learnLang3.learnArrayUtils;

import org.apache.commons.lang3.ArrayUtils;

import org.junit.jupiter.api.Test;

public class TestArray {

    @Test
    void test1() {
        int[] arr = {5, 8, 2, 3};

        boolean contains = ArrayUtils.contains(arr, 5);
        System.out.println("contains = " + contains);

        boolean contains2 = ArrayUtils.contains(arr, 1);
        System.out.println("contains2 = " + contains2);
    }
}
