package org.ziegler.javabase.learnLang3.validate;

import java.util.Objects;

import org.junit.jupiter.api.Test;

public class learnLang3Validate {

    @Test
    void testRequiredNoneNull() {
        testInteger(null);
    }

    void testInteger(Integer n) {
        Objects.requireNonNull(n, "n is null");
    }
}
