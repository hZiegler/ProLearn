package org.ziegler.javabase.math;

public class Test {
    @org.junit.jupiter.api.Test
    void testDivide() {
        int a = 25;
        int b = 8;
        final int i = a / b;
        System.out.println("i0 = " + i);
    }

    @org.junit.jupiter.api.Test
    void testDivide1() {
        int a = 24;
        int b = 8;
        final int i = a / b;
        System.out.println("i1 = " + i);
    }

    @org.junit.jupiter.api.Test
    void testDivide2() {
        int a = 23;
        int b = 8;
        final int i = a / b;
        System.out.println("i2 = " + i);
    }

    @org.junit.jupiter.api.Test
    void testDivide3() {
        int a = 0;
        int b = 8;
        final int i = a / b;
        System.out.println("i3 = " + i);
    }
}
