package org.ziegler.javabase.learnCollections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class testList {

    @Test
    void testSubList() {

        List<Integer> list = new ArrayList<>(List.of(1, 2, 3, 4, 5));
        List<Integer> subList = new ArrayList<>(list.subList(0, 3));
//        List<Integer> subList = list.subList(0, 3);

        System.out.println("init");

        System.out.println("subList:");
        subList.forEach(System.out::println);
        System.out.println("list:");
        list.forEach(System.out::println);

        list.removeAll(subList);

        System.out.println("after removeAll");

        System.out.println("subList:");
        subList.forEach(System.out::println);
        System.out.println("list:");
        list.forEach(System.out::println);

        list.clear();

        System.out.println("after clear");
        subList.forEach(System.out::println);
    }

    @Test
    void testLinkList() {
        LinkedList<Integer> list = new LinkedList<>(List.of(1, 2, 3, 4, 5));

        assertTrue(list.contains(3));

        assertTrue(list.contains(Integer.valueOf(3)));


        LinkedList<Long> list2 = new LinkedList<>(List.of(0L, 1L, 2L, 3L, 4L, 5L));

        long n = 0;
        assertTrue(list2.contains(n));

        assertTrue(list2.contains(Long.valueOf(3)));

        for (int i=0; i<2 && list2.size() > 0; ++i) {
            Long poll = list2.poll();
            System.out.println("poll = " + poll);
        }

    }
}
