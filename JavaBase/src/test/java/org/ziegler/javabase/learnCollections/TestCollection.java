package org.ziegler.javabase.learnCollections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class TestCollection {

    @Test
    void test() {
        List<Integer> list = new ArrayList<>();
        list.addAll(List.of(2, 3, 5, 8));

        list.forEach(System.out::println);

        list.replaceAll(n -> n * 2);

        list.forEach(System.out::println);

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "nk");
        map.put(3, "nos1");
        map.put(5, "jsk");

        map.merge(4, "game", (oldValue, newValue) -> {
            if ("nos".equals(oldValue)) {
                return "nos" + "game";
            }

            return "nos" + "-";
        });

        map.forEach((n, str) -> {
            System.out.println("n = " + n);
            System.out.println("str = " + str);
        });
    }
}
