package org.ziegler.javabase.learnProxy;

import java.util.List;

public interface UserMapper {
    List<Integer> getList();
}
