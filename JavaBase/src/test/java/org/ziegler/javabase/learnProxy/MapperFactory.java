package org.ziegler.javabase.learnProxy;

import java.lang.reflect.Proxy;

public class MapperFactory {

    public static UserMapper createUserMapper() {
        MyMapperProxy myMapperProxy = new MyMapperProxy();

        return (UserMapper) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[]{UserMapper.class},
                myMapperProxy);
    }

    public static <T> T getMapper(Class<T> type) {
        MyMapperProxy myMapperProxy = new MyMapperProxy();

        return (T) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[]{type},
                myMapperProxy);
    }
}
