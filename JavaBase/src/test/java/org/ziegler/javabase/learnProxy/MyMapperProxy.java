package org.ziegler.javabase.learnProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

public class MyMapperProxy implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<Integer> list = List.of(1, 3, 5);
        return list;
    }
}
