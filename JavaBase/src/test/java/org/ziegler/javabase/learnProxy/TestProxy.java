package org.ziegler.javabase.learnProxy;

import java.util.List;

import org.junit.jupiter.api.Test;

public class TestProxy {

    @Test
    void test() {
        UserMapper userMapper = MapperFactory.createUserMapper();

        List<Integer> list = userMapper.getList();
        System.out.println("list = " + list);
    }

    @Test
    void testGetMapper() {
        UserMapper userMapper = MapperFactory.getMapper(UserMapper.class);

        List<Integer> list = userMapper.getList();
        System.out.println("list = " + list);
    }

}
