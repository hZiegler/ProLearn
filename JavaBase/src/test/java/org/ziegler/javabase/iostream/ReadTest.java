package org.ziegler.javabase.iostream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

public class ReadTest {

    @Test
    void testRead() throws IOException {
        File file = new File("a.txt");

        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write("abc");
        } finally {
            System.out.println("文件读取完毕");
        }

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            final String s = bufferedReader.readLine();
            System.out.println("s = " + s);
        } finally {
            System.out.println("文件读取完毕");
        }

    }

    @Test
    void testRead2() throws IOException {
        File file = new File("a.txt");

//        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
//            bufferedWriter.write("abc");
//        } finally {
//            System.out.println("文件读取完毕");
//        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            final String s = bufferedReader.readLine();
            System.out.println("s = " + s);
        } finally {
            System.out.println("文件读取完毕");
        }

    }

//    public static void main(String[] args) throws IOException {
//        File file = new File("a.txt");
//
//        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
//            bufferedWriter.write("abc");
//        } finally {
//            System.out.println("文件读取完毕");
//        }
//
//        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
//            final String s = bufferedReader.readLine();
//            System.out.println("s = " + s);
//        } finally {
//            System.out.println("文件读取完毕");
//        }
//    }

    public static void main(String[] args) throws IOException {
        File file = new File("a.txt");

        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write("abc");
        } finally {
            System.out.println("文件读取完毕");
        }

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            final String s = bufferedReader.readLine();
            System.out.println("s = " + s);
        } finally {
            System.out.println("文件读取完毕");
        }
    }


}
