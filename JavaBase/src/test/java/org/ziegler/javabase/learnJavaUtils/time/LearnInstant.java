package org.ziegler.javabase.learnJavaUtils.time;

import java.time.Instant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LearnInstant {
    @Test
    void test() {
        Instant instant = Instant.now();

        Instant instant2 = Instant.ofEpochMilli(System.currentTimeMillis());

        System.out.println("instant = " + instant);
        System.out.println("instant.getEpochSecond() = " + instant.getEpochSecond());
        System.out.println("instant2 = " + instant2);
        System.out.println("instant2.getEpochSecond() = " + instant2.getEpochSecond());

        Assertions.assertEquals(instant.getEpochSecond(), instant2.getEpochSecond());

    }
}
