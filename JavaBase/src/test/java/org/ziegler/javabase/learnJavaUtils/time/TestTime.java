package org.ziegler.javabase.learnJavaUtils.time;

import java.time.Instant;

import org.apache.commons.lang3.time.StopWatch;

import org.junit.jupiter.api.Test;

public class TestTime {
    @Test
    void test1() throws InterruptedException {

        Instant now = Instant.now();

        {
            long epochSecond = now.getEpochSecond();
            System.out.println("epochSecond = " + epochSecond);
            int nano = now.getNano();
            System.out.println("nano = " + nano);
        }

        Thread.sleep(2000);

        Instant now2 = Instant.now();

        {
            long epochSecond = now2.getEpochSecond();
            System.out.println("epochSecond = " + epochSecond);
            int nano = now2.getNano();
            System.out.println("nano = " + nano);
        }

        boolean after = now.plusSeconds(1).isAfter(now2);
        System.out.println("after = " + after);
        boolean before = now.plusSeconds(1).isBefore(now2);
        System.out.println("before = " + before);
        boolean after1 = now.plusSeconds(3).isAfter(now2);
        System.out.println("after1 = " + after1);
    }

    @Test
    void test2() {
        StopWatch watch = StopWatch.createStarted();

        long n = 0;
        for (int i=0; i<10000000; ++i) {
            n += Instant.now().getNano();
        }

        long time = watch.getTime();
        System.out.println("time = " + time);
        System.out.println("n = " + n);
    }

    @Test
    void test3() {
        StopWatch watch = StopWatch.createStarted();

        long n = 0;
        for (int i=0; i<10000000; ++i) {
            n += System.currentTimeMillis();
        }

        long time = watch.getTime();
        System.out.println("time = " + time);
        System.out.println("n = " + n);
    }
}
