package org.ziegler.javabase.learnString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import org.junit.jupiter.api.Test;

public class TestString {

    @Test
    void testString() {
        String s1 = "abc";
        String s2 = s1.intern();
        String s3 = "bce";
        String s4 = "cba";

        System.out.println("s1 = " + s1);
        System.out.println("s2 = " + s2);
        System.out.println("s3 = " + s3);
        System.out.println("s4 = " + s4);
    }

    @Test
    void testJoin() {

        List<Integer> filterGroup = new ArrayList<>();
        filterGroup.add(3);
        filterGroup.add(5);
        filterGroup.add(1);

        String filterGroupStr = ArrayUtils.toString(filterGroup);
        System.out.println("filterGroupStr = " + filterGroupStr);



        System.out.println("ArrayUtils.toString(88) = " + ArrayUtils.toString(88));
    }

    @Test
    void testJoin2() {
        final String joinStr = List.of(1, 3, 5, 6).stream().map(n -> String.valueOf(n))
                .collect(Collectors.joining(","));
        System.out.println("joinStr = " + joinStr);
    }

    @Test
    void testStringJoin() {
        final String join = StringUtils.join(List.of(1, 5, 8), ",");
        System.out.println("join = " + join);
    }

    @Test
    void testJoinSet() {

        Set<Integer> filterGroup = new HashSet<>();
        filterGroup.add(3);
        filterGroup.add(5);
        filterGroup.add(1);

        String filterGroupStr = ArrayUtils.toString(filterGroup);
        System.out.println("filterGroupStr = " + filterGroupStr);



        System.out.println("ArrayUtils.toString(88) = " + ArrayUtils.toString(88));
    }

    @Test
    void testToInt() {
        int n = NumberUtils.toInt("");
        System.out.println("n = " + n);
    }

    @Test
    void testToInt2() {
        int n = Integer.valueOf("");
        System.out.println("n = " + n);
    }

    @Test
    void testToInt3() {
        int n = Integer.parseInt("");
        System.out.println("n = " + n);
    }

    @Test
    void testAssert() {

        assert false : "assert";


    }

    @Test
    void testEquals() {
        final String aa = "aa";
        final String aa1 = "aa1";
        final boolean equals = Objects.equals(aa1, aa);
        System.out.println("equals = " + equals);
    }
}
