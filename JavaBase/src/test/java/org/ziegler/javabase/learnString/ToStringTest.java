package org.ziegler.javabase.learnString;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import org.junit.jupiter.api.Test;

public class ToStringTest {

    private int id = 0;

    @Test
    void testDEFAULT_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("DEFAULT_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testMULTI_LINE_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("MULTI_LINE_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testNO_FIELD_NAMES_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("NO_FIELD_NAMES_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testSHORT_PREFIX_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("SHORT_PREFIX_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testSIMPLE_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("SIMPLE_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testNO_CLASS_NAME_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE);
        toStringBuilder.append("id", "111").append("name").append("jack");
        System.out.println("NO_CLASS_NAME_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void testJSON_STYLE() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.JSON_STYLE);
        toStringBuilder.append("id", 88).append("name", "jack");
//        toStringBuilder.append("id").append("111").append("name").append("jack");
        System.out.println("JSON_STYLE = " + toStringBuilder.toString());
    }

    @Test
    void test2() {
        final String stateType = new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE).append("confStation.sn", 1001)
                .append("stateType", 1).toString();
        System.out.println("stateType = " + stateType);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).toString();
    }
}
