package org.ziegler.javabase.testCommon;

public class Test {

    @org.junit.jupiter.api.Test
    void test1() {

        int diplomacyValue = 50;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }

    @org.junit.jupiter.api.Test
    void test2() {

        int diplomacyValue = -1;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }

    @org.junit.jupiter.api.Test
    void test3() {

        int diplomacyValue = 0;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }

    @org.junit.jupiter.api.Test
    void test4() {

        int diplomacyValue = 1000;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }

    @org.junit.jupiter.api.Test
    void test5() {

        int diplomacyValue = 1500;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }

    @org.junit.jupiter.api.Test
    void test6() {

        int diplomacyValue = 1;

        diplomacyValue = Math.max(0, diplomacyValue);
        diplomacyValue = Math.min(diplomacyValue, 1000);

        System.out.println("diplomacyValue = " + diplomacyValue);

    }
}
