package org.ziegler.optional;

import java.util.Optional;

public class Test {

    class A {
        int n;
        String name;

        A(int n, String name) {
            this.n = n;
            this.name = name;
        }
    }

    @org.junit.jupiter.api.Test
    void test() {
//        Optional<String> optInt = Optional.of("5");
        Optional<String> optInt = Optional.empty();
        Integer integer = optInt.map(str -> Integer.valueOf(str))
                                .map(n -> String.valueOf(n * n))
                                .map(str -> Integer.valueOf(str) + 1).orElse(0);
        System.out.println("integer = " + integer);

        Integer integer1 = optInt.flatMap(str -> Optional.of(Integer.valueOf(str)))
                                 .flatMap(n -> Optional.of(n * n)).orElse(999);
        System.out.println("integer1 = " + integer1);

        Optional<A> opA = Optional.of(new A(5, "game"));
        opA.map(a -> {
            a.n += 1;

            return a;
        });
    }

    public static void main(String[] args) {
        System.out.println("Test.main");

        for (int i = 0; i < 10; i++) {
            System.out.println("i = " + i);
        }


    }

    @org.junit.jupiter.api.Test
    void testOrElseGet() {

        Optional<Integer> opValue = Optional.of(8);

        Integer valueOrElseGet = opValue.orElseGet(this::getDefault);
        System.out.println("valueOrElseGet = " + valueOrElseGet);

        Integer valueOrElse = opValue.orElse(getDefault());
        System.out.println("valueOrElse = " + valueOrElse);
    }

    private int getDefault() {
        System.out.println("Test.getDefault");
        return -1;
    }
}
