package org.ziegler.intStream;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

public class IntStreamAndFor {

    @Test
    void testFor() {
        for (int i = 0; i < 10; i++) {
            System.out.println("i = " + i);
        }
    }

    @Test
    void testIntStream() {
        IntStream.range(0, 10).forEach(i -> System.out.println("i = " + i));
    }
}
