package org.ziegler.zcommand;

import org.ziegler.zunit.ModGm;
import org.ziegler.zunit.UnionTradeZUnit;

public class ZCommandStart {
    public static void main(String[] args) {
        {
            ZCommand zCommand = ZCommandFactory.createArgHumanObj(
                    new UnionTradeZUnit("test3"));

            // 下面将Command对象传递给被测试线程，由被测试线程回调run方法
            ModGm.run(zCommand);
        }

        {
            ZCommand zCommand = ZCommandFactory.createArgHumanObj(
                    new UnionTradeZUnit(""));

            // 下面将Command对象传递给被测试线程，由被测试线程回调run方法
            ModGm.run(zCommand);
        }
    }
}
