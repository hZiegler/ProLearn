package org.ziegler.zcommand;

import org.ziegler.zunit.ZUnit;
import org.ziegler.zunit.ZUnitHumanObj;

public class ZCommandFactory {

    public static ZCommand createArgHumanObj(ZUnitHumanObj zUnitHumanObject) {
        return new ZCommandArgHumanObj(zUnitHumanObject);
    }

    public static ZCommand createArg0(ZUnit zUnit) {
        return new ZCommandArg0(zUnit);
    }
}
