package org.ziegler.zcommand;

import org.ziegler.zunit.HumanObject;
import org.ziegler.zunit.ZUnitHumanObj;

public class ZCommandArgHumanObj implements ZCommand {

    private final ZUnitHumanObj zUnitHumanObj;

    public ZCommandArgHumanObj(ZUnitHumanObj zUnitHumanObj) {
        this.zUnitHumanObj = zUnitHumanObj;
    }

    @Override
    public void run(HumanObject humanObj) {
        zUnitHumanObj.test(humanObj);
    }

    @Override
    public void run() {

    }
}
