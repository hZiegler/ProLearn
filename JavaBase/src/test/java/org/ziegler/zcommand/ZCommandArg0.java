package org.ziegler.zcommand;

import org.ziegler.zunit.HumanObject;
import org.ziegler.zunit.ZUnit;

public class ZCommandArg0 implements ZCommand {

    private final ZUnit zUnit;

    public ZCommandArg0(ZUnit zUnit) {
        this.zUnit = zUnit;
    }

    @Override
    public void run() {
        zUnit.test();
    }

    @Override
    public void run(HumanObject humanObj) {

    }
}
