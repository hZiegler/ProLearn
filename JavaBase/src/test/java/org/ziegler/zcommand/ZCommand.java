package org.ziegler.zcommand;

import org.ziegler.zunit.HumanObject;

public interface ZCommand {
    /**
     * 没有参数的{@code run}方法
     */
    void run();

    /**
     * 1个参数{@link HumanObject}的{@code run}方法
     */
    void run(HumanObject humanObj);

}
