package org.ziegler.robot;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class RobotTest {

    static void doRobot() throws AWTException {
        final Robot robot = new Robot();

        // 按键PrintScreen
        robot.keyPress(KeyEvent.VK_PRINTSCREEN);
        robot.keyRelease(KeyEvent.VK_PRINTSCREEN);

        robot.delay(1000);

        // 鼠标左键单击
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

        // 按键Ctrl+V
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_V);

        robot.delay(1000);

        // 按键Enter
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }


    public static void main(String[] args) throws InterruptedException, AWTException {
        while (true) {

            doRobot();

            final int millis = 15 * 60 * 1000;
            Thread.sleep(millis);
        }
    }
}
