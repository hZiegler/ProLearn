package org.ziegler.org.ziegler.effectiveJava.cp2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.Test;

class UtilityClassTest {

    @Test
    void test() {
        // 无法new对象，因为private是私有方法
//        UtilityClass utilityClass = new UtilityClass();
    }

    @Test
    void testThroughReflection() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        final Constructor<UtilityClass> declaredConstructor = UtilityClass.class.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);

        final UtilityClass newInstance = declaredConstructor.newInstance();

    }
}