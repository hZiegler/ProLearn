package org.ziegler.stream;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class TestCollectors {

    @Test
    void testJoining() {
        String result = Stream
                .iterate(1, n -> n + 1)
                .limit(10)
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining(", ", "[", "]"));

        System.out.println("result = " + result);
    }

    @Test
    void testJoining2() {
        String result = Stream
                .iterate(1, n -> n + 1)
                .limit(10)
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining(", "));

        System.out.println("result = " + result);
    }

    @Test
    void testJoining3() {
        String result = Stream
                .iterate(1, n -> n + 1)
                .limit(10)
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining());

        System.out.println("result = " + result);
    }

}
