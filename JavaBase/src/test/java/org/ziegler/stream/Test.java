package org.ziegler.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;


public class Test {

    @org.junit.jupiter.api.Test
    public void test1() {


        Comparator<Integer> comparator = Comparator.comparing((Integer n) -> n >= 4 ? n : n + 2)
                .thenComparing((Integer m) -> m + 2);

        int compare = comparator.compare(3, 4);
        System.out.println(compare);


    }

    @org.junit.jupiter.api.Test
    void testCount() {

        List<String> testList = new ArrayList<>();
        testList.add("name");
        testList.add("jack");
        testList.add("tom");
        testList.add("hz");

        long count = testList.stream().filter(str -> str.length() > 3).count();
        System.out.println("count = " + count);


        Stream<String> stream = testList.stream();
        stream.forEach(s -> System.out.println(s));
        //stream.forEach(s -> System.out.println(s));

    }

    @org.junit.jupiter.api.Test
    void testGroupBy() {

        List<Integer> list = List.of(1, 3, 5, 7, 9);

        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 101);
        map.put(3, 103);
        map.put(5, 105);
        map.put(7, 107);
        map.put(9, 109);


        Map<Integer, Integer> result = list.stream().collect(
                toMap(n -> n, n -> map.getOrDefault(n, 0)));

        System.out.println("result = " + result);


    }


    @org.junit.jupiter.api.Test
    void testMapAndFlatMap() {
        List<String> words = List.of("Hello", "World");
        List<String> cList = words
                .stream()
                .map(w -> w.split(""))
                .flatMap(strArr -> Arrays.stream(strArr))
                .distinct()
                .collect(toList());

        System.out.println("cList = " + cList);
    }

    @org.junit.jupiter.api.Test
    void testFlatMap() {
        List<Integer> num1 = Arrays.asList(1, 2, 3);
        List<Integer> num2 = Arrays.asList(10, 100, 1000);

        List<int[]> results = num1.stream()
                                    .flatMap(i -> num2.stream().map(j -> new int[] {i, j, i * j}))
                                    .collect(toList());

        results.forEach(arr -> System.out.println(arr[0] + " * " + arr[1] + " = " + arr[2]));
    }


    @org.junit.jupiter.api.Test
    void testIntStream() {
        IntStream.range(0, 5).forEach(n -> System.out.println("n = " + n));
    }

    @org.junit.jupiter.api.Test
    void testPairToInteger() {
        List<Pair<Integer, Integer>> skillLevel = new ArrayList<>();
        skillLevel.add(ImmutablePair.of(1001, 2));
        skillLevel.add(ImmutablePair.of(1002, 1));
        skillLevel.add(ImmutablePair.of(1003, 5));
        skillLevel.add(ImmutablePair.of(1002, 4));
        final List<Integer> skills = skillLevel.stream()
                .map(pair -> pair.getKey())
                .collect(Collectors.toList());

        skills.forEach(skillSn -> System.out.println("skillSn = " + skillSn));
    }
}
