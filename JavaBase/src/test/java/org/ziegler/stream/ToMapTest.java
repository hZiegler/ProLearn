package org.ziegler.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import org.junit.jupiter.api.Test;

/**
 * 
 *
 * @author Ziegler
 * date 2020/8/15
 */
public class ToMapTest {

    @Test
    void testToMap() {
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        list.add(ImmutablePair.of(1, 100));
        list.add(ImmutablePair.of(1, 101));
        list.add(ImmutablePair.of(1, 102));
        list.add(ImmutablePair.of(2, 101));
        list.add(ImmutablePair.of(2, 102));

        final Map<Integer, Integer> map = list.stream()
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        map.forEach((k, v) -> {
            System.out.println("k = " + k + " : v = " + v);
        });
    }

    @Test
    void testToMapMerge() {
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        list.add(ImmutablePair.of(1, 100));
        list.add(ImmutablePair.of(1, 101));
        list.add(ImmutablePair.of(1, 102));
        list.add(ImmutablePair.of(2, 101));
        list.add(ImmutablePair.of(2, 102));

        // 使用merge
        final Map<Integer, Integer> map = list.stream()
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue, (v1, v2) -> v2));

        map.forEach((k, v) -> {
            System.out.println("k = " + k + " : v = " + v);
        });
    }

    @Test
    void testListAddAll() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(null);
        list.add(4);

        List<Integer> cpList = new ArrayList<>();
        cpList.addAll(list);
        
        cpList.forEach(n -> System.out.println("n = " + n));

    }

    @Test
    void testCollectorsToMap() {
        Map<Integer, Integer> partSkillLevelMap = Map.of(5, 3, 10, 5);
        List<Integer> skillSnList = List.of(1, 5, 8, 10);
        final Map<Integer, Integer> result = skillSnList.stream()
                .collect(Collectors.toMap(skillSn -> skillSn, skillSn -> partSkillLevelMap.getOrDefault(skillSn, 1)));
        result.forEach((key, value) -> {
            System.out.println("key = " + key + ", value = " + value);
        });
    }

    @Test
    void testCollectorsToMap2() {
        Map<Integer, Integer> partSkillLevelMap = Collections.emptyMap();
        List<Integer> skillSnList = List.of(1, 5, 8, 10);
        final Map<Integer, Integer> result = skillSnList.stream()
                .collect(Collectors.toMap(skillSn -> skillSn, skillSn -> partSkillLevelMap.getOrDefault(skillSn, 1)));
        result.forEach((key, value) -> {
            System.out.println("key = " + key + ", value = " + value);
        });
    }

}
