package org.ziegler.db.loadAndSave;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuestManagerTest {

    @Test
    void testCommit() {
        QuestDB questDB = new QuestDB(1, 0);
        QuestManager questManager = new QuestManager(questDB);
        questManager.init();

        questManager.submit();
    }

    @Test
    void testGiveUp() {
        QuestDB questDB = new QuestDB(1, 0);
        QuestManager questManager = new QuestManager(questDB);
        questManager.init();

        questManager.giveUp();
    }

    @Test
    void testInit1() {
        QuestDB questDB = new QuestDB(1, 1);
        QuestManager questManager = new QuestManager(questDB);
        questManager.init();

        questManager.submit();
    }

    @Test
    void testInit2() {
        QuestDB questDB = new QuestDB(1, 2);
        QuestManager questManager = new QuestManager(questDB);
        questManager.init();

        questManager.submit();
    }

    @Test
    void testNewQuest() {
        QuestDB questDB = new QuestDB(1, 0);
        QuestManager questManager = new QuestManager(questDB);
        questManager.init();

        questManager.newQuest(3);
        questManager.submit();
    }



}