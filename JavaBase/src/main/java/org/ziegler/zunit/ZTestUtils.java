package org.ziegler.zunit;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.reflect.MethodUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class ZTestUtils {

    /** 测试组件 */
    private static final String UNION_TRADE_Z_UNIT = "org.ziegler.zunit.ZUnitExecutor";

    /** 日志模块名称 */
    private static final String LOG_MAIN_NAME = "ZUnitTest";

    private static final Logger logger = LoggerFactory
            .getLogger(LOG_MAIN_NAME);

    public static void test(HumanObject humanObj) {
        try {
            final Class<?> UnionTradeZUnit = Class.forName(UNION_TRADE_Z_UNIT);
            MethodUtils.invokeExactStaticMethod(UnionTradeZUnit, "test", humanObj);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException e) {
            logger.error("通过反射调用函数失败。{}", e.toString());
        }
    }

    public static void test(HumanObject humanObj, String methodName) {
        try {
            final Class<?> UnionTradeZUnit = Class.forName(UNION_TRADE_Z_UNIT);
            MethodUtils.invokeExactStaticMethod(UnionTradeZUnit, "test", humanObj, methodName);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException e) {
            logger.error("通过反射调用函数失败。{}", e.toString());
        }
    }

}
