package org.ziegler.zunit;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

public class ZCommandUtils {

    /** 日志模块名称 */
    private static final String LOG_MAIN_NAME = "ZUnitTest";

    private static final Logger logger = LoggerFactory
            .getLogger(LOG_MAIN_NAME);

    public static void invokeCommandRun(Object objCommand) {
        try {
            MethodUtils.invokeMethod(objCommand, "run");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error("通过反射调用函数失败。{}", e.toString());
        }
    }

    public static void invokeCommandRun(Object objCommand, HumanObject humanObj) {
        try {
            MethodUtils.invokeMethod(objCommand, "run", humanObj);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error("通过反射调用函数失败。{}", e.toString());
        }
    }
}
