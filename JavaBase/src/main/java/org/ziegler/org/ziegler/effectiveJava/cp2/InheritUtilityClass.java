package org.ziegler.org.ziegler.effectiveJava.cp2;

/**
 * 无法继承，因为构造函数private
 *
 * @auther Ziegler
 * @date 2020/6/29 10:49
 */
public class InheritUtilityClass /*extends UtilityClass*/ {
}
