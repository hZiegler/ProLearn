package org.ziegler.javabase.thread.inherit;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class InheritClass extends BaseClass {

    int value = 0;
    String desc;

    public InheritClass() {
        System.out.println("InheritClass construct");
    }

    @Override
    protected void init() {
        super.init();

        /** 下面的复制操作会被变量的初始复制给覆盖掉。
         * 因为init是在父类构造函数调用，当前类的成员数据还没有初始化，
         * 等到当前成员初始化时，会把下面的数据赋值又给覆盖掉。*/
        value = 8;
        desc = "InheritClass";

        System.out.println("InheritClass.init");
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("value", value)
                .append("desc", desc)
                .toString();
    }
}
