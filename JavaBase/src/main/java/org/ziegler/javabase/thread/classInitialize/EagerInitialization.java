package org.ziegler.javabase.thread.classInitialize;

import net.jcip.annotations.ThreadSafe;

/**
 * 提前初始化
 *
 * @auther Ziegler
 * @date 2020/6/24 20:42
 */
@ThreadSafe
public class EagerInitialization {
    private static Resource resourceE = new Resource();

    public static Resource getResourceE() {
        return resourceE;
    }
}
