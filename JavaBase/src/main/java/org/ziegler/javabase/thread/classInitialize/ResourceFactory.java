package org.ziegler.javabase.thread.classInitialize;


import net.jcip.annotations.ThreadSafe;

/**
 * 延迟初始化占位模式(推荐)
 *
 * @auther Ziegler
 * @date 2020/6/24 20:45
 */
@ThreadSafe
public class ResourceFactory {
    private static class ResourceHolder {
        public static Resource resource = new Resource();
    }

    public static Resource getResource() {
        return ResourceHolder.resource;
    }
}
