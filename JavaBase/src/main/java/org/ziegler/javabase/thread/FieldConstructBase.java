package org.ziegler.javabase.thread;

public abstract class FieldConstructBase {

    private int id = 3;

    public FieldConstructBase() {
        init();
    }

    abstract void init();
}
