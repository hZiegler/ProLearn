package org.ziegler.javabase.thread.single;

public class LockTwice {

    public static void main(String[] args) {
        Role role1 = new Role();
        role1.setName("jack");

        Role role2 = new Role();
        role2.setName("tom");

        Role role3 = role1;

        synchronized (role1) {
            synchronized (role2) {
                System.out.println("role1.getName() = " + role1.getName());
                System.out.println("role2.getName() = " + role2.getName());
            }
        }
    }
}
