package org.ziegler.javabase.thread.inherit;

public class BaseClass {

    int id = 0;
    String name = "";

    public BaseClass() {
        System.out.println("BaseClass construct");
        /**
         * 千万不在构造函数中调用重载方法，成员数据初始化赋值顺序和
         * 构造函数调用顺序，导致奇怪的问题。
         */
        init();
    }

    protected void init() {
        System.out.println("BaseClass.init");
    }

}
