package org.ziegler.javabase.thread.doubleEnterLock;

public class Widget {
    public synchronized void doSomething() {
        System.out.println("Widget.doSomething");
    }

    public void doOtherThing() {
        synchronized (this) {
            synchronized (this) {
                System.out.println("Widget.doOtherThing");
            }
        }
    }
}

