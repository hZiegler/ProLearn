package org.ziegler.javabase.thread.single;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ShutdownTest {

    ExecutorService exec = Executors.newSingleThreadExecutor();

    public static void main(String[] args) throws InterruptedException {
        ShutdownTest shutdownTest = new ShutdownTest();
        shutdownTest.test();

    }

    void test() throws InterruptedException {
        exec.execute(() -> {
            // sleep
            System.out.println("before execute");
            try {
                Thread.sleep(3000);
                System.out.println("after execute");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println("before shutdown");
        // 会等待线程完成
        exec.shutdown();
        exec.awaitTermination(10, TimeUnit.SECONDS);

        // 会中断线程
//        exec.shutdownNow();

        System.out.println("after shutdown");
    }
}
