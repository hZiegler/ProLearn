package org.ziegler.javabase.thread.classInitialize;

import net.jcip.annotations.ThreadSafe;

/**
 * 线程安全的延迟初始化
 *
 * @auther Ziegler
 * @date 2020/6/24 20:42
 */
@ThreadSafe
public class SafeLayInitialization {

    private static Resource resource;

    public synchronized static Resource getInstance() {
        if (resource == null) {
            resource = new Resource();
        }

        return resource;
    }

}
