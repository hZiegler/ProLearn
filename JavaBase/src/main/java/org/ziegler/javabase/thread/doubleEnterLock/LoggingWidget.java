package org.ziegler.javabase.thread.doubleEnterLock;

public class LoggingWidget extends Widget {

    @Override
    public synchronized void doSomething() {
        System.out.println("LoggingWidget.doSomething");
        super.doSomething();
    }
}
