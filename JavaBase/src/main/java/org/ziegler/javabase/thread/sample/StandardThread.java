package org.ziegler.javabase.thread.sample;

public class StandardThread extends Thread {
    @Override
    public void run() {
        Throwable thrown = null;
        try {
            while (!isInterrupted()) {
                runTask();
            }
        } catch (Exception e) {
            thrown = e;
        } finally {
            threadExited(this, thrown);
        }
    }

    private void threadExited(StandardThread standardThread, Throwable thrown) {
    }

    private void runTask() {
    }
}
