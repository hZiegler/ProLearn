package org.ziegler.javabase.thread;

import java.util.concurrent.TimeUnit;

public class DelayUtil {
    public static void millis(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
//            e.printStackTrace();
            // 中断
            System.out.println("DelayUtil.millis");
        }
    }

    public static void second(long second) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(second));
        } catch (InterruptedException e) {
//            e.printStackTrace();
            // 中断
            System.out.println("DelayUtil.second");
        }
    }
}
