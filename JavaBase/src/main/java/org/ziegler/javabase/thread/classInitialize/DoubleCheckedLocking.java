package org.ziegler.javabase.thread.classInitialize;

import net.jcip.annotations.ThreadSafe;

/**
 * 双重检查加锁
 *
 * @auther Ziegler
 * @date 2020/6/24 20:50
 */
@ThreadSafe
public class DoubleCheckedLocking {
    /** 注意volatile，不然就不是线程安全的 */
    private static volatile Resource resource;

    public static Resource getResource() {
        if (resource == null) {
            synchronized (DoubleCheckedLocking.class) {
                if (resource != null) {
                    resource = new Resource();
                }
            }
        }
        return resource;
    }
}
