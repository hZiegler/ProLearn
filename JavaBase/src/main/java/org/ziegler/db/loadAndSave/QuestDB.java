package org.ziegler.db.loadAndSave;

public class QuestDB {

    private int questSn;

    private int questStateIndex;

    public QuestDB(int questSn, int questStateIndex) {
        this.questSn = questSn;
        this.questStateIndex = questStateIndex;
    }

    public int getQuestSn() {
        return questSn;
    }

    public void setQuestSn(int questSn) {
        this.questSn = questSn;
        System.out.println("DB save questSn = " + questSn);
    }

    public int getQuestStateIndex() {
        return questStateIndex;
    }

    public void setQuestStateIndex(QuestStateType questStateType) {
        this.questStateIndex = questStateType.getIndex();

        System.out.println("DB save questStateIndex = " + questStateIndex+ ", questStateType = " + questStateType);
    }
}
