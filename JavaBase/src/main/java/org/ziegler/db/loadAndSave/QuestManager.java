package org.ziegler.db.loadAndSave;

public class QuestManager {

    private final QuestDB questDB;

    Quest curQuest;

    public QuestManager(QuestDB questDB) {
        this.questDB = questDB;
    }

    void init() {
        QuestStateType questStateType = QuestStateType.indexOf(questDB.getQuestStateIndex());
        Quest quest = new Quest(questDB.getQuestSn(), questStateType, this);
        quest.init();

        if (quest.isDestroy()) {
            return ;
        }

        curQuest = quest;
    }

    void submit() {
        if (curQuest != null) {
            curQuest.submit();

            if (curQuest.isDestroy()) {
                curQuest = null;
            }
        } else {
            System.out.println("任务为空，没有任务提交");
        }
    }

    void giveUp() {
        if (curQuest != null) {
            curQuest.giveUp();
            curQuest = null;
        } else {
            System.out.println("任务为空，没有任务放弃");
        }
    }

    void newQuest(int questSn) {
        // 任务覆盖，放弃上一个任务
        giveUp();

        Quest quest = new Quest(questSn, QuestStateType.PREPARE, this);
        quest.init();
        if (quest.isDestroy()) {
            return ;
        }

        curQuest = quest;
    }

    public QuestDB getQuestDB() {
        return questDB;
    }
}
