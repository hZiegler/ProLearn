package org.ziegler.db.loadAndSave;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 任务状态
 * @author Ziegler
 * date : 2020/7/25 16:18
 */
public enum QuestStateType {
    PREPARE(0),
    SUBMITTED(1),
    REWARD(2),
    FINISH(3),
    DESTROY(4),
    ;

    private final int index;

    QuestStateType(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("name", name())
                .append("index", index)
                .toString();
    }

    public static QuestStateType indexOf(int index) {
        for (QuestStateType value : values()) {
            if (value.getIndex() == index) {
                return value;
            }
        }

        return PREPARE;
    }
}
