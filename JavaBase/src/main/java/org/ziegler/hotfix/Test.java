package org.ziegler.hotfix;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Test {

    public static boolean fetched = false;

    public static void main(String[] args) throws IOException {

        final Scanner scanner = new Scanner(System.in);

        scanner.hasNextLine();

        // 清空
        HotFixUtils.writeMultiline("out.txt", Collections.emptyList(), false);

        while (true) {
            final String s = scanner.nextLine();
            if (StringUtils.isEmpty(s)) {
                break;
            }

            final String[] rowStrArray = s.split("\t");
            final int rowSplitCount = rowStrArray.length;
            if (rowSplitCount >= 3) {
                final String sheetFullName = rowStrArray[1];
                final String[] sheetFullNameArray = sheetFullName.split("\\|");
                if (sheetFullNameArray.length != 2) {
                    System.out.println("Sheet页名错误 s = " + sheetFullNameArray + ", s = " + s);
                    return ;
                }

                String sheetName = sheetFullNameArray[1];
                String fixConfigFileName = sheetName + ".fixconfig.txt";

                if (!new File(fixConfigFileName).exists()) {
                    System.out.println("忽略掉不存在的fixconfig文件 = " + fixConfigFileName + ", s = " + s);
                    continue;
                }

                final List<String> result;

                if (rowSplitCount == 3) {
                    String compareValue = rowStrArray[2];

                    FixConfigReader fixConfigReader = new FixConfigReader(fixConfigFileName);
                    result = fixConfigReader.match(sRow -> {
                        final String splitFirstValue = HotFixUtils.getSplitFirstValue(sRow);
                        final String key = StringUtils.trim(splitFirstValue);
                        return key.trim().equals(compareValue.trim());
                    });
                } else {
                    String topValue = rowStrArray[2];
                    String bottomValue = rowStrArray[3];

                    FixConfigReader fixConfigReader = new FixConfigReader(fixConfigFileName);
                    result = fixConfigReader.match(sRow -> {
                        final String splitFirstValue = HotFixUtils.getSplitFirstValue(sRow);
                        final String key = StringUtils.trim(splitFirstValue);

                        if (!fetched) {
                            if (key.trim().equals(topValue)) {
                                fetched = true;
                            }
                        } else {
                            if (key.trim().equals(bottomValue)) {
                                fetched = false;
                                return true;
                            }
                        }

                        return fetched;
                    });

                }


                result.forEach(System.out::println);

                HotFixUtils.writeMultiline("out.txt", result, true);
            } else {
                System.out.println("行参数个数错误. s = " + s);
            }
        }

        System.out.println("全部处理完成");



    }

}
