package org.ziegler.hotfix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * *.fixconfig.txt文件读取
 *
 * @author Ziegler
 * date 2020/9/23
 */
public class FixConfigReader {
    private final FileReader fileReader;
    private final BufferedReader bufferedReader;

    private int minRangeValue = 0;
    private int maxRangeValue = 0;

    public FixConfigReader(String fileName) throws FileNotFoundException {
        fileReader = new FileReader(new File(fileName));
        bufferedReader = new BufferedReader(fileReader);
    }

    /**
     * 所有匹配的行
     * @param filter
     * @return
     * @throws IOException
     */
    public List<String> match(Predicate<String> filter) throws IOException {
        final List<String> allLine = new ArrayList<>();

        String str = bufferedReader.readLine();
        while (str != null) {
            if (filter.test(str)) {
                allLine.add(str);
            }

            // 读取下一行
            str = bufferedReader.readLine();
        }

        return allLine;
    }


}
