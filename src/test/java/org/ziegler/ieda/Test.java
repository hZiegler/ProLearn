package org.ziegler.ieda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Test {

    public int getCalculate() {
        ArrayList<Integer> listInt = new ArrayList<>();
        HashMap<Integer, String> mapStr = new HashMap<>();
        HashSet<LineItem> setLineItem = new HashSet<>();

        int total = listInt.stream().mapToInt(val -> val).sum();

        total += mapStr.keySet().stream().mapToInt(i -> i).sum();

        total += setLineItem.stream().mapToInt(LineItem::getValue).sum();

        return total;
    }
}
