package org.ziegler.learnPattern.factory;

public interface UnitObject {

    String getName();

    void pulse();
}
