package org.ziegler.learnPattern.factory;

public class MonsterObject implements UnitObject {
    @Override
    public String getName() {
        return "MonsterObject";
    }

    @Override
    public void pulse() {

    }
}
