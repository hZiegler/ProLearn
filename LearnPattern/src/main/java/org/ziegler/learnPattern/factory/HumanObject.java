package org.ziegler.learnPattern.factory;

public class HumanObject implements UnitObject {
    @Override
    public String getName() {
        return "HumanObject";
    }

    @Override
    public void pulse() {

    }

}
