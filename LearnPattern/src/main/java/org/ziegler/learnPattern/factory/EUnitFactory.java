package org.ziegler.learnPattern.factory;

public enum EUnitFactory {
    MONSTER {
        public MonsterObject create() {
            return new MonsterObject();
        }
    },
    NPC {
        public NPCObject create() {
            return new NPCObject();
        }
    },
    HUMAN {
        @Override
        public HumanObject create() {
            return new HumanObject();
        }
    },

    ;

    public abstract UnitObject create();
}
