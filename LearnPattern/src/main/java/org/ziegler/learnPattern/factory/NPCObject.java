package org.ziegler.learnPattern.factory;

public class NPCObject implements UnitObject {
    @Override
    public String getName() {
        return "NPCObject";
    }

    @Override
    public void pulse() {

    }
}
