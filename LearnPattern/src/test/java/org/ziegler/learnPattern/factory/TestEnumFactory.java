package org.ziegler.learnPattern.factory;

import org.junit.jupiter.api.Test;

public class TestEnumFactory {

    @Test
    void test() {

        UnitObject humanObj = EUnitFactory.HUMAN.create();
        System.out.println("humanObj.getName() = " + humanObj.getName());

        UnitObject monsterObj = EUnitFactory.MONSTER.create();
        System.out.println("monsterObj.getName() = " + monsterObj.getName());

        UnitObject npcObj = EUnitFactory.NPC.create();
        System.out.println("npcObj.getName() = " + npcObj.getName());

    }
}
