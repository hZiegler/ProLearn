package org.ziegler.mybatis;

import org.apache.ibatis.session.SqlSession;

import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.mapper.PrivilegeMapper;
import org.ziegler.mybatis.model.SysPrivilege;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PrivilegeMapperTest extends BaseMapperTest {

    @Test
    void testPrivilegeSelect() {
        try(SqlSession sqlSession = getSqlSession()) {
            PrivilegeMapper sysRoleMapper = sqlSession.getMapper(PrivilegeMapper.class);
            SysPrivilege sysPrivilege = sysRoleMapper.selectById(1L);

            assertNotNull(sysPrivilege);

            System.out.println("sysPrivilege = " + sysPrivilege);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
