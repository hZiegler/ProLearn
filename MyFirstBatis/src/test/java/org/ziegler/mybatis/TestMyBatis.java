package org.ziegler.mybatis;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.model.User;

public class TestMyBatis {

    private static SqlSessionFactory sqlSessionFactory;

    @BeforeAll
    static void beforeAll() {

        try(InputStream inMyBatis = Resources.getResourceAsStream("mybatis.xml")) {
            try(InputStream inProperties = Resources.getResourceAsStream("config.properties")) {
                Properties ppt = new Properties();
                ppt.load(inProperties);
                inProperties.close();

                sqlSessionFactory = new SqlSessionFactoryBuilder().build(inMyBatis, ppt);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("beforeAll SqlSessionFactory init complete.");
    }

    @AfterAll
    static void afterAll() {

        System.out.println("afterAll");
    }

    @Test
    public void testFirst() {

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            User user = sqlSession.selectOne("UserMapper.selectUser", "ziegler");
            System.out.println(user.toString());

            User user2 = sqlSession.selectOne("UserMapper.selectUser", "game");
            System.out.println(user2.toString());

            User user3 = sqlSession.selectOne("UserMapper.selectUser", "jack");
            System.out.println(user3.toString());
        }
    }



}
