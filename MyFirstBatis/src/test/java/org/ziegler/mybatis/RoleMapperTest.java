package org.ziegler.mybatis;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.mapper.SysRoleMapper;
import org.ziegler.mybatis.model.SysRole;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoleMapperTest extends BaseMapperTest {



    @Test
    void testSelectRoleId() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysRoleMapper sysRoleMapper = sqlSession.getMapper(SysRoleMapper.class);
            SysRole sysRole = sysRoleMapper.selectById(1L);

            assertNotNull(sysRole);

            System.out.println("sysRole = " + sysRole);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSelectAll() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysRoleMapper sysRoleMapper = sqlSession.getMapper(SysRoleMapper.class);
            List<SysRole> sysRoleList = sysRoleMapper.selectAll();

            assertNotNull(sysRoleList);

            assertTrue(sysRoleList.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void testPrivilegeSelect() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysRoleMapper sysRoleMapper = sqlSession.getMapper(SysRoleMapper.class);
            List<SysRole> sysRoleList = sysRoleMapper.selectAll();

            assertNotNull(sysRoleList);

            assertTrue(sysRoleList.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
