package org.ziegler.mybatis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.mapper.SysUserMapper;
import org.ziegler.mybatis.model.SysRole;
import org.ziegler.mybatis.model.SysUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserMapperTest extends BaseMapperTest {

    @Test
    void testUTF8() {
        System.out.println("字符编码");
    }

    @Test
    void testSelectById() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);
            SysUser sysUser = sysUserMapper.selectById(1L);

            System.out.println("sysUser.getUserInfo() = " + sysUser.getUserInfo());

            assertNotNull(sysUser);

            assertEquals("admin", sysUser.getUserName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSelectAll() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);
            List<SysUser> sysUserList = sysUserMapper.selectAll();

            assertNotNull(sysUserList);

            assertTrue(sysUserList.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSelectRolesByUserId() {
        try(SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);
            List<SysRole> sysRoleList = sysUserMapper.selectRolesByUserId(1L);

            assertNotNull(sysRoleList);

            assertTrue(sysRoleList.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testInsert() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser = new SysUser();
            sysUser.setUserName("test1");
            sysUser.setUserPassword("123456");
            sysUser.setUserEmail("test@qq.com");
            sysUser.setUserInfo("程序员");
            sysUser.setHeadImg(new byte[]{1, 2, 3});
            sysUser.setCreateTime(new Date());

            int result = sysUserMapper.insert(sysUser);

            assertEquals(1, result);

            System.out.println("sysUser.getId() = " + sysUser.getId());

            assertNull(sysUser.getId());

            //sqlSession.commit();

            sqlSession.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void testInsert2() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser = new SysUser();
            sysUser.setUserName("test1");
            sysUser.setUserPassword("123456");
            sysUser.setUserEmail("test@qq.com");
            sysUser.setUserInfo("程序员");
            sysUser.setHeadImg(new byte[]{1, 2, 3});
            sysUser.setCreateTime(new Date());

            int result = sysUserMapper.insert2(sysUser);

            assertEquals(1, result);

            System.out.println("sysUser.getId() = " + sysUser.getId());

            assertNotNull(sysUser.getId());

            sqlSession.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testInsert3() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser = new SysUser();
            sysUser.setUserName("test1");
            sysUser.setUserPassword("123456");
            sysUser.setUserEmail("test@qq.com");
            sysUser.setUserInfo("程序员");
            sysUser.setHeadImg(new byte[]{1, 2, 3});
            sysUser.setCreateTime(new Date());

            int result = sysUserMapper.insert3(sysUser);

            assertEquals(1, result);

            System.out.println("sysUser.getId() = " + sysUser.getId());

            assertNotNull(sysUser.getId());

            sqlSession.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testUpdateById() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser = sysUserMapper.selectById(1L);

            System.out.println("sysUser.getUserInfo() = " + sysUser.getUserInfo());

            assertEquals("admin", sysUser.getUserName());

            sysUser.setUserName("admin_test");

            sysUser.setUserEmail("test@mybatis.org");

            sysUser.setUserInfo("程序员");

            int result = sysUserMapper.updateById(sysUser);

            assertEquals(1, result);

            SysUser sysUserUpdated = sysUserMapper.selectById(1L);

            assertEquals("admin_test", sysUserUpdated.getUserName());

            System.out.println("sysUserUpdated.getUserInfo() = " + sysUserUpdated.getUserInfo());

            sqlSession.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void testDeleteById() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser1 = sysUserMapper.selectById(1L);

            assertNotNull(sysUser1);

            assertEquals(1, sysUserMapper.deleteById(1L));

            assertNull(sysUserMapper.selectById(1L));

            SysUser sysUser2 = sysUserMapper.selectById(1001L);

            assertNotNull(sysUser2);

            assertEquals(1, sysUserMapper.deleteById(sysUser2));

            assertNull(sysUserMapper.selectById(1001L));

            sqlSession.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSelectRolesByUserIdAndRoleEnabled() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);
            List<SysRole> sysRoles = sysUserMapper.selectRolesByUserIdAndRoleEnabled(1L, 1);

            assertNotNull(sysRoles);

            assertTrue(sysRoles.size() > 0);
        }
    }

    @Test
    void testSelectByUser() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            {
                // 只查询用户名时
                SysUser query = new SysUser();
                query.setUserName("ad");
                List<SysUser> sysUserList = sysUserMapper.selectByUser(query);
                assertTrue(sysUserList.size() > 0);
            }


            {
                // 只查询用户邮箱
                SysUser query = new SysUser();
                query.setUserEmail("test@qq.com");
                List<SysUser> sysUserList = sysUserMapper.selectByUser(query);
                assertTrue(sysUserList.size() > 0);
            }

            {
                // 同时查询用户名和邮箱
                SysUser query = new SysUser();
                query.setUserName("ad");
                query.setUserEmail("test@qq.com");
                List<SysUser> sysUserList = sysUserMapper.selectByUser(query);
                assertTrue(sysUserList.size() == 0);
            }
        }
    }

    @Test
    void testUpdateByIdSelective() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser user = new SysUser();
            user.setId(1L);
            user.setUserEmail("test@qq.com");

            int result = sysUserMapper.updateByIdSelective(user);
            assertEquals(1, result);

            user = sysUserMapper.selectById(1L);

            assertEquals("admin", user.getUserName());
            assertEquals("test@qq.com", user.getUserEmail());

        } finally {
            sqlSession.rollback();
            sqlSession.close();
        }

    }

    @Test
    void testInsert2Selective() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysUserMapper sysUserMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser sysUser = new SysUser();
            sysUser.setUserName("test1");
            sysUser.setUserPassword("123456");
            sysUser.setUserInfo("程序员");
            sysUser.setHeadImg(new byte[]{1, 2, 3});
            sysUser.setCreateTime(new Date());

            int result = sysUserMapper.insert2(sysUser);

            assertEquals(1, result);

            System.out.println("sysUser.getId() = " + sysUser.getId());

            assertNotNull(sysUser.getId());

            sysUser = sysUserMapper.selectById(sysUser.getId());
            assertEquals("test@qq.com", sysUser.getUserEmail());

        } finally {
            sqlSession.rollback();
            sqlSession.close();
        }
    }

    @Test
    void testSelectByIdOrUserName() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);

            SysUser query = new SysUser();
            query.setId(1L);
            query.setUserName("admin");
            SysUser user = userMapper.selectByIdOrUserName(query);

            assertNotNull(user);

            query.setId(null);
            user = userMapper.selectByIdOrUserName(query);
            assertNotNull(user);

            query.setUserName(null);
            user = userMapper.selectByIdOrUserName(query);
            assertNull(user);
        } finally {
            sqlSession.rollback();
            sqlSession.close();
        }
    }

    @Test
    void testSelectByIdList() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);

            List<Long> idList = new ArrayList<>();
            idList.add(1L);
            idList.add(1001L);
            List<SysUser> userList = userMapper.selectByIdList(idList);
            assertEquals(2, userList.size());

        } finally {
            sqlSession.rollback();
            sqlSession.close();
        }
    }

    @Test
    void testInsertList() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);

            List<SysUser> userList = new ArrayList<>();

            for (int i=0; i<2; ++i) {
                SysUser user = new SysUser();
                user.setUserName("test" + i);
                user.setUserPassword("123456");
                user.setUserEmail("test@qq.com");
                userList.add(user);
            }

            int result = userMapper.insertList(userList);
            assertEquals(2, result);

        } finally {
            sqlSession.rollback();
            sqlSession.close();
        }
    }

    @Test
    void testUpdateByMap() {
        try (SqlSession sqlSession = getSqlSession()) {
            SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);

            Map<String, Object> map = new HashMap<>();
            map.put("id", 1L);
            map.put("user_email", "test1@qq.com");
            map.put("user_password", "12345678");

            userMapper.updateByMap(map);

            SysUser user = userMapper.selectById(1L);

            assertEquals("test1@qq.com", user.getUserEmail());

            sqlSession.rollback();
        }
    }
}
