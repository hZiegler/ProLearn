package org.ziegler.mybatis;

import org.apache.ibatis.session.SqlSession;

import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.mapper.SysRoleMapper;
import org.ziegler.mybatis.mapper.SysUserMapper;
import org.ziegler.mybatis.model.SysRole;
import org.ziegler.mybatis.model.SysUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CacheTest extends BaseMapperTest {

    @Test
    void testL1Cache() {
        SysUser user1;
        try (SqlSession sqlSession = getSqlSession()) {
            final SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);
            user1 = userMapper.selectById(1L);
            user1.setUserName("New Name");
            final SysUser user2 = userMapper.selectById(1L);
            assertEquals("New Name", user2.getUserName());
            assertEquals(user1, user2);
        }

        System.out.println("开启新的sqlSession");

        try (SqlSession sqlSession = getSqlSession()) {
            final SysUserMapper userMapper = sqlSession.getMapper(SysUserMapper.class);
            final SysUser user2 = userMapper.selectById(1L);
            assertNotEquals(user1, user2);
            userMapper.deleteById(2L);
            final SysUser user3 = userMapper.selectById(1L);
            assertNotEquals(user2, user3);
        }
    }

    @Test
    void testL2Cache() {
        SysRole role1;
        try (SqlSession sqlSession = getSqlSession()) {
            final SysRoleMapper roleMapper = sqlSession.getMapper(SysRoleMapper.class);
            role1 = roleMapper.selectById(1L);
            role1.setRoleName("New Name");
            final SysRole role2 = roleMapper.selectById(1L);
            assertEquals("New Name", role2.getRoleName());
            assertEquals(role1, role2);
        }

        System.out.println("开启新的sqlSession");

        try (SqlSession sqlSession = getSqlSession()) {
            final SysRoleMapper roleMapper = sqlSession.getMapper(SysRoleMapper.class);
            final SysRole role2 = roleMapper.selectById(1L);
            assertEquals("New Name", role2.getRoleName());
            assertNotEquals(role1, role2);
            final SysRole role3 = roleMapper.selectById(1L);
            assertNotEquals(role2, role3);

        }
    }
}
