package org.ziegler.mybatis;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;

import org.junit.jupiter.api.Test;
import org.ziegler.mybatis.service.Main;

public class TestLoadText {

    @Test
    public void testLoadText() {
        System.out.println(Main.class.getResource("/").getPath());

        try (InputStream resourceAsStream = Resources.getResourceAsStream("config.properties")) {

            byte[] bytes = new byte[100];
            resourceAsStream.read(bytes);

            System.out.println(new String(bytes));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
