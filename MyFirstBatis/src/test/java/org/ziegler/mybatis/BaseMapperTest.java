package org.ziegler.mybatis;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.junit.jupiter.api.BeforeAll;

public class BaseMapperTest {
    private static SqlSessionFactory sqlSessionFactory;

    @BeforeAll
    static void beforeAll() {

        try(InputStream inMyBatis = Resources.getResourceAsStream("mybatis.xml")) {
            try(InputStream inProperties = Resources.getResourceAsStream("config.properties")) {
                Properties ppt = new Properties();
                ppt.load(inProperties);
                inProperties.close();

                sqlSessionFactory = new SqlSessionFactoryBuilder().build(inMyBatis, ppt);
//                final SimpleInterceptor interceptor1 = new SimpleInterceptor("拦截器1");
//                sqlSessionFactory.getConfiguration().addInterceptor(interceptor1);

//                final SimpleInterceptor interceptor2 = new SimpleInterceptor("拦截器2");
//                sqlSessionFactory.getConfiguration().addInterceptor(interceptor2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("beforeAll SqlSessionFactory init complete.");
    }

    public static SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }
}
