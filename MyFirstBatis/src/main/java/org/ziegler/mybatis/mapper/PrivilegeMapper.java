package org.ziegler.mybatis.mapper;

import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.jdbc.SQL;

import org.ziegler.mybatis.model.SysPrivilege;

public interface PrivilegeMapper {

    @SelectProvider(type = PrivilegeProvider.class, method="selectById")
    SysPrivilege selectById(Long id);

    class PrivilegeProvider {
        public String selectById(final Long id) {
            return new SQL() {
                {
                    SELECT("id, privilege_name privilegeName, privilege_url privilegeUrl");
                    FROM("sys_privilege");
                    WHERE("id = #{id}");
                }
            }.toString();
        }
    }
}
