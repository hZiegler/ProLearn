package org.ziegler.mybatis.mapper;

import org.apache.ibatis.annotations.Param;
import org.ziegler.mybatis.model.SysRole;
import org.ziegler.mybatis.model.SysUser;

import java.util.List;
import java.util.Map;


public interface SysUserMapper {
    SysUser selectById(Long id);

    List<SysUser> selectAll();

    List<SysRole> selectRolesByUserId(Long userId);

    int insert(SysUser sysUser);

    int insert2(SysUser sysUser);

    int insert3(SysUser sysUser);

    int updateById(SysUser sysUser);

    int deleteById(Long id);

    int deleteById(SysUser sysUser);

    List<SysRole> selectRolesByUserIdAndRoleEnabled(
            @Param("userId") Long userId,
            @Param("enabled") Integer enabled);

    List<SysUser> selectByUser(SysUser sysUser);

    int updateByIdSelective(SysUser sysUser);

    SysUser selectByIdOrUserName(SysUser sysUser);

    List<SysUser> selectByIdList(@Param("ids") List<Long> idList);

    int insertList(List<SysUser> userList);

    int updateByMap(Map<String, Object> map);

}
