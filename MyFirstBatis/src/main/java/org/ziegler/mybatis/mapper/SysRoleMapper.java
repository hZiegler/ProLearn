package org.ziegler.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import org.ziegler.mybatis.model.SysRole;

//@CacheNamespace(
//        eviction = FifoCache.class,
//        flushInterval = 60000,
//        size = 512,
//        readWrite = true
//)
//@CacheNamespaceRef(SysUserMapper.class)
public interface SysRoleMapper {

    @Results(id = "roleResultMap", value = {
            @Result(property = "id", column="id", id=true),
            @Result(property = "roleName", column="role_name"),
            @Result(property = "enabled", column="enabled"),
            @Result(property = "createBy", column="create_by"),
            @Result(property = "createTime", column="create_time")
    })
    @Select("select id, role_name, enabled, create_by, create_time " +
            "from sys_role where id = #{id}")
    SysRole selectById(Long id);

    @ResultMap("roleResultMap")
    @Select("select * from sys_role")
    List<SysRole> selectAll();

}
