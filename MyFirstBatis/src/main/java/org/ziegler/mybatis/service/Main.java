package org.ziegler.mybatis.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.ziegler.mybatis.model.User;


public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        //main.test2();
        main.test();
    }

    void test() {

        System.out.println(Resources.class.getResource("/").getPath());

        try(InputStream inMyBatis = Resources.getResourceAsStream("mybatis.xml")) {
            try(InputStream inProperties = Resources.getResourceAsStream("config.properties")) {
                Properties ppt = new Properties();
                ppt.load(inProperties);
                inProperties.close();

                SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inMyBatis, ppt);
                try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                    User user = sqlSession.selectOne("UserMapper.selectUser", "ziegler");
                    System.out.println(user.toString());

                    User user2 = sqlSession.selectOne("UserMapper.selectUser", "game");
                    System.out.println(user2.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
