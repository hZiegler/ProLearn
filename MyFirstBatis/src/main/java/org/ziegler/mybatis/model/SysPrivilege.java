package org.ziegler.mybatis.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SysPrivilege {

    private Long id;
    private String privilegeName;

    private String privilegeUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public String getPrivilegeUrl() {
        return privilegeUrl;
    }

    public void setPrivilegeUrl(String privilegeUrl) {
        this.privilegeUrl = privilegeUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("privilegeName", privilegeName)
                .append("privilegeUrl", privilegeUrl)
                .toString();
    }
}
