package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.TooLongFrameException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SafeByteToMessageDecoderTest {
    @Test
    void test() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 1000; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new SafeByteToMessageDecoder());
        try {
            embeddedChannel.writeInbound(input);
            Assertions.fail();
        } catch (Exception e) {
            // 异常
            System.out.println("e.getMessage() = " + e.getMessage());
        }

        assertFalse(embeddedChannel.finish());
    }
}