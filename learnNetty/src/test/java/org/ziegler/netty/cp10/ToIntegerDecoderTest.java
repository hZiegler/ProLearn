package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ToIntegerDecoderTest {

    @Test
    void test() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new ToIntegerDecoder());
        assertTrue(embeddedChannel.writeInbound(input));
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            int n = embeddedChannel.readInbound();
            assertEquals(n, i);
//            System.out.println("n = " + n);
        }
    }
}