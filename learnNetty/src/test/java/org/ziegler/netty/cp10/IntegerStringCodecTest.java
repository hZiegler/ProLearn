package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegerStringCodecTest {

    @Test
    void test() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new IntegerStringCodec());
        assertTrue(embeddedChannel.writeInbound(input));
        for (int i = 0; i < 10; i++) {
            assertTrue(embeddedChannel.writeOutbound(i+100));
        }
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            int n = embeddedChannel.readInbound();
            assertEquals(n, i);
            System.out.println("n = " + n);
        }

        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = embeddedChannel.readOutbound();
            int value = byteBuf.readInt();
            assertEquals(i+100, value);
            System.out.println("value = " + value);
        }

    }
}