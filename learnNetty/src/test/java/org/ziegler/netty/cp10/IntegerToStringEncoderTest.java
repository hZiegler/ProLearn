package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegerToStringEncoderTest {

    @Test
    void test() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new IntegerToStringEncoder());

        for (int i = 0; i < 10; i++) {
            assertTrue(embeddedChannel.writeOutbound(i));
        }
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            String value = embeddedChannel.readOutbound();
            assertEquals(String.valueOf(i), value);

            System.out.println("value = " + value);
        }

        assertNull(embeddedChannel.readOutbound());
    }

}