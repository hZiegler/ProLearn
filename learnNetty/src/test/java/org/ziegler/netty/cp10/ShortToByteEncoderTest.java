package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortToByteEncoderTest {

    @Test
    void test() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new ShortToByteEncoder());
        for (short i = 0; i < 10; i++) {
            assertTrue(embeddedChannel.writeOutbound(i));
        }
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = embeddedChannel.readOutbound();
            short sValue = byteBuf.readShort();
            assertEquals(i, sValue);

            System.out.println("sValue = " + sValue);
        }

        assertNull(embeddedChannel.readOutbound());
    }
}