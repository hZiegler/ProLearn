package org.ziegler.netty.cp10;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegerToStringDecoderTest {

    @Test
    void test() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new ChannelInitializer() {
            @Override
            protected void initChannel(Channel ch) throws Exception {
                ch.pipeline().addLast(new ToIntegerDecoder())
                        .addLast(new IntegerToStringDecoder());
            }
        });
        assertTrue(embeddedChannel.writeInbound(input));
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            String intValue = (String)embeddedChannel.readInbound();
            assertEquals(intValue, String.valueOf(i));
            System.out.println("intValue = " + intValue);
        }
    }

    @Test
    void testAddFirst() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel();
        // 添加Handler方法1
        embeddedChannel.pipeline()
                .addFirst(new IntegerToStringDecoder())
                .addFirst(new ToIntegerDecoder());
        assertTrue(embeddedChannel.writeInbound(input));
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            String intValue = (String)embeddedChannel.readInbound();
            assertEquals(intValue, String.valueOf(i));
            System.out.println("intValue = " + intValue);
        }
    }

    @Test
    void testAddLast() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buf.writeInt(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel embeddedChannel = new EmbeddedChannel();
//        // 添加Handler方法2
        embeddedChannel.pipeline()
                .addLast(new ToIntegerDecoder())
                .addLast(new IntegerToStringDecoder());
        assertTrue(embeddedChannel.writeInbound(input));
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            String intValue = (String)embeddedChannel.readInbound();
            assertEquals(intValue, String.valueOf(i));
            System.out.println("intValue = " + intValue);
        }
    }
}