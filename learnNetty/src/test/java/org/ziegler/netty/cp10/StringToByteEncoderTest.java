package org.ziegler.netty.cp10;

import com.sun.jdi.Value;
import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringToByteEncoderTest {

    @Test
    void test() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new StringToByteEncoder());
        for (int i = 0; i < 10; i++) {
            assertTrue(embeddedChannel.writeOutbound(String.valueOf(i)));
        }
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = embeddedChannel.readOutbound();
            int readableBytes = byteBuf.readableBytes();
            byte[] bytes = new byte[readableBytes];
            byteBuf.readBytes(bytes);
            String s = new String(bytes);
            assertTrue(s.equals(String.valueOf(i)));
        }

        assertNull(embeddedChannel.readOutbound());
    }

    @Test
    void testSendIntString() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel();
        embeddedChannel.pipeline()
                .addFirst(new IntegerToStringEncoder())
                .addFirst(new StringToByteEncoder());

        for (int i = 0; i < 10; i++) {
            assertTrue(embeddedChannel.writeOutbound(i));
        }
        assertTrue(embeddedChannel.finish());

        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = embeddedChannel.readOutbound();
            int readableBytes = byteBuf.readableBytes();
            byte[] bytes = new byte[readableBytes];
            byteBuf.readBytes(bytes);
            String s = new String(bytes);
            assertTrue(s.equals(String.valueOf(i)));
            System.out.println("s = " + s);
        }

        assertNull(embeddedChannel.readOutbound());
    }
}