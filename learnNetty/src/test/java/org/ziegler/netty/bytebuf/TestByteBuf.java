package org.ziegler.netty.bytebuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;

public class TestByteBuf {

    @Test
    void testBuffArray() {
        ByteBuf heapBuf = Unpooled.copiedBuffer("hello wrold!", Charset.forName("UTF8"));
        if (heapBuf.hasArray()) {
            byte[] array = heapBuf.array();
            int offset = heapBuf.arrayOffset() + heapBuf.readerIndex();
            int length = heapBuf.readableBytes();

            handleArray(array, offset, length);
        }
    }

    @Test
    void testDirectBuf() {
        ByteBuf directBuf = Unpooled.directBuffer();
        if (!directBuf.hasArray()) {
            int length = directBuf.readableBytes();
            byte[] array = new byte[length];
            directBuf.getBytes(directBuf.readerIndex(), array);
            handleArray(array, 0, length);
        }
    }

    private void handleArray(byte[] array, int i, int length) {
        System.out.println("offset = " + i);
        System.out.println("length = " + length);
    }

    @Test
    void testCompositeByteBuf() {
        CompositeByteBuf compositeByteBuf = Unpooled.compositeBuffer();
        int length = compositeByteBuf.readableBytes();
        byte[] array = new byte[length];
        compositeByteBuf.getBytes(compositeByteBuf.readerIndex(), array);

        handleArray(array, 0, length);
    }

    @Test
    void testSlice() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf sliced = buf.slice(0, 15);
        System.out.println(sliced.toString());
        buf.setByte(0, (byte)'J');
        assert buf.getByte(0) == sliced.getByte(0);
    }


    @Test
    void testCopy() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf sliced = buf.copy(0, 15);
        System.out.println(sliced.toString());
        buf.setByte(0, (byte)'J');
        assert buf.getByte(0) != sliced.getByte(0);
    }

    @Test
    void testGetSet() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        System.out.println((char)buf.getByte(0));
        int readerIndex = buf.readerIndex();
        int writerIndex = buf.writerIndex();
        buf.setByte(0, 'B');
        System.out.println((char)buf.getByte(0));
        assert readerIndex == buf.readerIndex();
        assert writerIndex == buf.writerIndex();

        System.out.println("readerIndex = " + readerIndex);
        System.out.println("writerIndex = " + writerIndex);
        System.out.println("buf.toString() = " + new String(buf.array()));
    }

    @Test
    void testReadWrite() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        System.out.println((char)buf.readByte());
        int readerIndex = buf.readerIndex();
        int writerIndex = buf.writerIndex();
        buf.writeByte('?');
        assert readerIndex == buf.readerIndex();
        assert writerIndex != buf.writerIndex();

        System.out.println("readerIndex = " + readerIndex);
        System.out.println("writerIndex = " + writerIndex);
        System.out.println("buf.toString() = " + new String(buf.array()));
    }

    @Test
    void testUnpooled() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        Unpooled.wrappedBuffer(buf);
    }
}
