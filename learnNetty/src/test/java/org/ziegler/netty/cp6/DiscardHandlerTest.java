package org.ziegler.netty.cp6;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.ziegler.netty.cp9.FrameChunkDecoder;

import static org.junit.jupiter.api.Assertions.*;

class DiscardHandlerTest {

    public static void main(String[] args) {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 9; i++) {
            buf.writeByte(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel channel = new EmbeddedChannel(new DiscardHandler());
        assertTrue(channel.writeInbound(input.readBytes(2)));
        assertTrue(channel.writeInbound(input.readBytes(4)));
        assertTrue(channel.writeInbound(input.readBytes(3)));
        assertTrue(channel.finish());

        ByteBuf read = channel.readInbound();
        assertEquals(buf.readSlice(2), read);

        read = channel.readInbound();
        assertEquals(buf.readSlice(4), read);
    }

    @Test
    void testLeak() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 9; i++) {
            buf.writeByte(i);
        }

        ByteBuf input = buf.duplicate();

        EmbeddedChannel channel = new EmbeddedChannel(new DiscardHandler());
        assertTrue(channel.writeInbound(input.readBytes(2)));
        assertTrue(channel.writeInbound(input.readBytes(4)));
        assertTrue(channel.writeInbound(input.readBytes(3)));
        assertTrue(channel.finish());

        ByteBuf read = channel.readInbound();
        assertEquals(buf.readSlice(2), read);
        read.release();

        read = channel.readInbound();
        assertEquals(buf.readSlice(4), read);
        read.release();
    }
}