package org.ziegler.netty.cp8;

public class StartUp {

    public static void main(String[] args) throws InterruptedException {
        int thirdPort = 8888;
        ThirdServer thirdServer = new ThirdServer();
        thirdServer.start(thirdPort);

        int serverPort = 8080;
        SharedEventLoopServer server = new SharedEventLoopServer();
        server.start(serverPort, "127.0.0.1", thirdPort);

        TestClient testClient = new TestClient();
        testClient.connect("127.0.0.1", serverPort);
    }
}
