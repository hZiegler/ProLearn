package org.ziegler.netty.cp8;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;

import java.net.InetSocketAddress;

public class SharedEventLoopServer {

    public static void main(String[] args) throws InterruptedException {
        int thirdPort = 8888;
        SharedEventLoopServer server = new SharedEventLoopServer();
        server.start(8080, "127.0.0.1", thirdPort);
    }

    void start(int port, String thirdIP, int thirdPort) {

        final AttributeKey<Integer> sid = AttributeKey.newInstance("SID");

        ServerBootstrap bootstrap = new ServerBootstrap();
        NioEventLoopGroup parentGroup = new NioEventLoopGroup();
        NioEventLoopGroup childGroup = new NioEventLoopGroup();
        bootstrap.group(parentGroup, childGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new SimpleChannelInboundHandler<ByteBuf>() {
                    @Override
                    public void channelActive(ChannelHandlerContext ctx) throws Exception {

                        System.out.println("server - ctx.channel().attr(id) = " + ctx.channel().attr(sid));

                        Bootstrap bootstrap1 = new Bootstrap();
                        bootstrap1.group(ctx.channel().eventLoop())
                                .channel(NioSocketChannel.class)
                                .handler(new SimpleChannelInboundHandler<ByteBuf>() {
                                    @Override
                                    public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                        System.out.println("connected to third server");
                                    }

                                    @Override
                                    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
                                        System.out.println("Received data");
                                    }
                                });
                        bootstrap1.connect(new InetSocketAddress(thirdIP, thirdPort));
                    }

                    @Override
                    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
                        System.out.println("Server Received data");
                    }
                });

        bootstrap.childAttr(sid, 789);

        ChannelFuture future = bootstrap.bind(new InetSocketAddress(port));
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("Server bound : " + port);
                } else {
                    System.out.println("Bind attempt failed");
                    future.cause().printStackTrace();
                }
            }
        });

//        parentGroup.shutdownGracefully();
//        childGroup.shutdownGracefully();

    }
}
