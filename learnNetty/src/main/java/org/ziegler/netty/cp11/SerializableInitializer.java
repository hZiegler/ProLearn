package org.ziegler.netty.cp11;

import io.netty.channel.*;
import io.netty.handler.codec.serialization.ClassResolver;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.util.concurrent.EventExecutorGroup;

public class SerializableInitializer extends ChannelInitializer<Channel> {

    private final ClassResolver classResolver;

    public SerializableInitializer(ClassResolver classResolver) {
        this.classResolver = classResolver;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new ObjectDecoder(classResolver));
        pipeline.addLast(new ObjectEncoder());
        pipeline.addLast(new ObjectHandler());
    }

    private static class ObjectHandler extends SimpleChannelInboundHandler<Object> {
        @Override
        protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
            // Do something
        }
    }
}
