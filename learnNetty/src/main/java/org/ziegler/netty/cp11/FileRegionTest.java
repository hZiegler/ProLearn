package org.ziegler.netty.cp11;

import io.netty.channel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FileRegionTest {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File(args[0]);
        Channel channel = null;


        FileInputStream in = new FileInputStream(file);
        FileRegion region = new DefaultFileRegion(in.getChannel(),
                0, file.length());
        channel.writeAndFlush(region).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (!future.isSuccess()) {
                    Throwable cause = future.cause();
                    // Do Something
                }
            }
        });

    }
}
