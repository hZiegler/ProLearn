package org.zigler.log4j2;


import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TestLog4j2Test {

    @Test
    void testLog() {
        final Logger logger = LoggerFactory.getLogger("HelloWorld");
        logger.trace("Hello, World!");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
    }

    @Test
    void testGame() {
        final Logger logger = LoggerFactory.getLogger("game");
        logger.trace("Hello, World!");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
    }

    @Test
    void testOther() {
        final Logger logger = LoggerFactory.getLogger("other");
        logger.trace("Hello, World!");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
    }


    @Test
    void testDebugEnabled() {
        final Logger logger = LoggerFactory.getLogger("other");
        if (logger.isDebugEnabled()) {
            logger.debug("hello {}{}", "world", "!");
        }
    }

    @Test
    void testSystemLOG() {
        final Logger logger = LoggerFactory.getLogger("sys");
        logger.info("in");
        logger.error("e");
    }

    @Test
    void testSystem() {
        System.out.println("test out");
        System.err.println("test err");
    }
}