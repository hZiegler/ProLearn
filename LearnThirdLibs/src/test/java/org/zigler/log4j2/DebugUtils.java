package org.zigler.log4j2;

import java.util.function.Function;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.LambdaUtil;
import org.apache.logging.log4j.util.Supplier;

import org.junit.jupiter.api.Test;

public class DebugUtils {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Test
    void test1() {
        log.error("error");
        log.info("info");
        log.debug("debug1");
        log.atLevel(Level.ERROR);
        log.debug("debug2");
        log.info("info");

//        log.debug();


    }

    @Test
    void testDelay() {
        log.debug("value = {}", () -> 5);
        debug("value = {}", () -> 6);
    }

    public void debug(String message, final Supplier<?>... paramSuppliers) {
        if (log.isDebugEnabled()) {
            log.debug(message, LambdaUtil.getAll(paramSuppliers));
        }
    }

    String callTest(int n) {
        System.out.println("DebugUtils.callTest");
        return String.valueOf(n);
    }

    @Test
    void testCall() {
        Function<Integer, String> f = this::callTest;
        final String apply = f.apply(8);
        System.out.println("apply = " + apply);
    }
}
