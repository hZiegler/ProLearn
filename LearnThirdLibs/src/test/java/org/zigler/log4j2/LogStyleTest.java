package org.zigler.log4j2;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Test;

public class LogStyleTest {
    @Test
    void test1() {
        final Logger logger = LogManager.getLogger("HelloWorld");
        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("id",5)
                .append("name", "jack")
                .toString());


        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                        .append("id",5)
                        .append("name", "jack")
                        .toString());

        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                        .append("id",5)
                        .append("name", "jack")
                        .toString());

        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                        .append("id",5)
                        .append("name", "jack")
                        .toString());

        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                        .append("id",5)
                        .append("name", "jack")
                        .toString());

        logger.error("错误参数合并。{}", () ->
                new ToStringBuilder(this)
                        .append("id",5)
                        .append("name", "jack")
                        .toString());


    }
}
