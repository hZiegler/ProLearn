package org.zigler.log4j2.multiClassLog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogClassA {
    private final Logger logger = LoggerFactory
            .getLogger(this.getClass());


    public void testLog() {
        logger.info("LogClassA.testLog");

        if (logger.isDebugEnabled()) {
            logger.debug("LogClassA.testLog");
        }
    }
}
