package org.zigler.log4j2.multiClassLog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogClassB {
    private static final Logger logger = LoggerFactory
            .getLogger(LogClassB.class);

    public static void testLog() {
        logger.info("LogClassB.testLog");

        if (logger.isDebugEnabled()) {
            logger.debug("LogClassB.testLog");
        }
    }
}
