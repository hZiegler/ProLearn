package org.zigler.log4j2.multiClassLog;

import org.junit.jupiter.api.Test;
import org.zigler.log4j2.multiClassLog.inner.LogClassInner;

public class TestLog {

    @Test
    void testLog() {
        {
            final LogClassA logClassA = new LogClassA();
            logClassA.testLog();

            final LogClassB logClassB = new LogClassB();
            logClassB.testLog();
        }
//
//        {
//            final LogClassA logClassA = new LogClassA();
//            logClassA.testLog();
//
//            final LogClassB logClassB = new LogClassB();
//            logClassB.testLog();
//        }

        final LogClassInner logClassInner = new LogClassInner();
        logClassInner.testLog();
    }
}
