package org.zigler.log4j2.multiClassLog.inner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogClassInner {

    private final Logger logger = LoggerFactory
            .getLogger(this.getClass());

    public void testLog() {
        logger.info("LogClassInner.testLog");

        if (logger.isDebugEnabled()) {
            logger.debug("LogClassInner.testLog");
        }
    }
}
