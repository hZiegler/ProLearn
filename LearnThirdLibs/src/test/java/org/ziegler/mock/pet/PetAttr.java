package org.ziegler.mock.pet;

import java.util.List;

public class PetAttr {
    private final long id;
    private final String name;

    public PetAttr(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getBaseSkills(boolean isTemp) {
        return List.of(1, 2, 5);
    }
}
