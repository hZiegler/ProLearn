package org.ziegler.mock.pet;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PetMock {

    @Test
    void name() {

        ModPet modPet = mock(ModPet.class);

        PetAttr petAttr = mock(PetAttr.class);

        when(modPet.getPet(anyLong())).thenReturn(petAttr);
//        when(petAttr.getName()).thenReturn("mockName");
        when(modPet.getPet(anyLong()).getName()).thenReturn("mockName2");
        when(petAttr.getBaseSkills(false)).thenReturn(List.of(2, 8));

        final PetAttr pet = modPet.getPet(100);
        System.out.println("modPet.getPet(100).getName() = " + pet.getName());

        final List<Integer> baseSkills = pet.getBaseSkills(false);
        baseSkills.forEach(System.out::println);


    }
}
