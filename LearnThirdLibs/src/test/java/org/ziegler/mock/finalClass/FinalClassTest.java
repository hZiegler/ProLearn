package org.ziegler.mock.finalClass;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FinalClassTest {


    @Test
    void name() {

        final FinalClass mock = mock(FinalClass.class);
        when(mock.getName()).thenReturn("game");

        System.out.println("mock.getName() = " + mock.getName());
    }
}
