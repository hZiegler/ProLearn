package org.ziegler.mock.staticMethod;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;

public class MockStaticTest {
    @Test
    void test() {
        assertEquals("foo", Foo.getName());

        try (MockedStatic<Foo> fooMockedStatic = mockStatic(Foo.class)) {
            fooMockedStatic.when(Foo::getName).thenReturn("mockFoo");

            System.out.println("Foo.getName() = " + Foo.getName());
        }
    }

    @Test
    void testM() {
        final Foo mock = mock(Foo.class);

        System.out.println("mock.getDesc() = " + mock.getDesc());
    }
}
