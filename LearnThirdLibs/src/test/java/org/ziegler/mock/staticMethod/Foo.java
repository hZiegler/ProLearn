package org.ziegler.mock.staticMethod;

public class Foo {

    //---------------------------------------- static fields ----------------------------------------

    private static String name = "foo";

    public static String getName() {
        return name;
    }

    public static void setName(String value) {
        name = value;
    }

    public static String getValueName(int value) {
        return name + value;
    }

    public static int getT(int ok) {
        return -1;
    }

    //---------------------------------------- class fields ----------------------------------------

    public String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static void test() {
        System.out.println("true = " + true);
    }
}
