package org.ziegler.mock;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

public class MockTest {

    @Test
    void name() {
        System.out.println("this.getClass().getResource(\"/\").toString() = "
                + MockTest.class.getResource("/").toString());

    }

    @Test
    void testMock() {
        List list = mock(List.class);

        list.add("one");
        list.clear();
    }
}
