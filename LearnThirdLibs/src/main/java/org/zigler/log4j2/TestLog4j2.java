package org.zigler.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestLog4j2 {

    public static void main(String[] args) {
        final Logger logger = LogManager.getLogger("HelloWorld");
        logger.info("Hello, World!");

    }
}
